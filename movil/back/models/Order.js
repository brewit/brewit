const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    userId: String,
    date: Date,
    cardLastFour: String,
    status: String,
    products: [{
        productId: String,
        name: String,
        origin: String,
        price: Number,
        photo: String,
        quantity: Number
    }],
    total: Number
}, { collection: 'Orders', versionKey: false });

const Order = mongoose.model('Order', OrderSchema);

module.exports = Order;
