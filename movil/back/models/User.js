const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: String,
    email: String,
    password: String,
    _id: String,
    birthdate: String,
    isAdmin: { type: Boolean, default: false },
    cart: [{
        productId: String,
        name: String,
        origin: String,
        price: Number,
        photo: String,
        quantity: Number
    }],
    orders: [{
        date: Date,
        cardLastFour: String,
        status: String,
        products: [{
            productId: String,
            name: String,
            origin: String,
            price: Number,
            photo: String,
            quantity: Number
        }],
        total: Number
    }]
}, { collection: 'Users', versionKey: false });

const User = mongoose.model('User', UserSchema);

module.exports = User;
