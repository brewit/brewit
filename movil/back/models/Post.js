const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        default: Date.now,
    },
    user: {
        //type: mongoose.Schema.Types.ObjectId,
        type: String,
        ref: 'User'
    },
    likes: [{
        type: String,
        ref: 'User'
    }],
    comments: [{
        type: String,
        ref: 'Comment',
    }],
});

module.exports = mongoose.model('Post', PostSchema);
