require('dotenv').config();
const express = require('express');
const { expressjwt: jwt } = require('express-jwt');
const app = express();
const port = process.env.PORT || 3001;
const UserRouter = require('./routes/User');
const productRouter = require('./routes/products');
const cartRouter = require('./routes/cart');
const orderRouter = require('./routes/order');
const paymentRouter = require('./routes/payment');
const postRouter = require('./routes/post');

// Reemplazar tu clave secreta con una variable de entorno
const secretKey = process.env.SECRET_KEY || 'secret-key';

// Autenticación JWT
app.use(jwt({
    secret: secretKey,
    algorithms: ['HS256'],
    credentialsRequired: false,
    getToken: function fromHeaderOrQuerystring(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}).unless({ path: ['/user/signup', '/user/signin', '/api/products'] }));

// Manejo de errores de JWT
app.use(function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send({status: "FAILED", message: 'Invalid token'});
    }
});

// mongoDB
const mongoose = require('mongoose');
mongoose
    .connect(process.env.MONGODB_URI, {
        useNewUrlParser: true, useUnifiedTopology: true,
    })
    .then(() => {
        console.log("MongoDB Connected");
    })
    .catch((err) => console.log(err));

// CORS
const cors = require('cors');
app.use(cors());

// for accepting post form data
app.use(express.json({limit: '10mb'}));
app.use(express.urlencoded({limit: '10mb', extended: true}));

// Para servir los archivos estáticos de la carpeta uploads
app.use('/uploads', express.static('uploads'));

// endpoints
app.use('/api/users', UserRouter);
app.use('/api/products', productRouter);
app.use('/api/cart', cartRouter);
app.use('/api/orders', orderRouter);
app.use('/api/payment', paymentRouter);
app.use('/api/posts', postRouter);

// Manejo de errores
app.use(function (err, req, res, next) {
    if (err instanceof multer.MulterError) {
      res.status(500).json({ status: "FAILED", message: "File upload error" });
    } else if (err.name === 'UnauthorizedError') {
      res.status(401).send({status: "FAILED", message: 'Invalid token'});
    } else {
      next(err);
    }
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
