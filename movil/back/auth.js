// auth.js
require('dotenv').config();
const jwt = require('jsonwebtoken');
const secretKey = process.env.SECRET_KEY;

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;;

    if (authHeader) {
        const token = authHeader.split(' ')[1]; // obtienes el token sin el prefijo 'Bearer'
        // Loguear el token que se recibe
        console.log('Token recibido: ', token);
        jwt.verify(token, secretKey, (err, user) => {
            if (err) {
                console.log('Error en la verificación del token: ', err);
                return res.sendStatus(403);
            }

            console.log('Token verificado correctamente, user: ', user);

            req.user = user;
            //console.log("User from JWT:", user);
            next();
        });
    } else {
        console.log('No se recibió token');
        res.sendStatus(401);
    }
};

module.exports = authenticateJWT;
