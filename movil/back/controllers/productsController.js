const path = require('path');
const fs = require('fs');
const Product = require('../models/Products');


module.exports = {
    createProduct: async(req, res) => {
        // Si multer procesó una imagen, entonces req.file debería existir
        if (req.file) {
            req.body.photo = `/uploads/${req.file.filename}`;
        }
        
        const newProduct = new Product(req.body);
        try {
            await newProduct.save();
            res.status(200).json("Product created successfully!")
        } catch (error) {
            console.error(error);
            res.status(500).json("Failed to create a product");
        }
    },

    updateProduct: async (req, res) => {
        try {
          const productToUpdate = await Product.findById(req.params.id);
    
          if (!productToUpdate) {
            return res.status(404).json("Product not found");
          }
    
          if (req.file) {
            // Si hay una imagen anterior, bórrala
            if (productToUpdate.photo) {
              const oldPhotoPath = path.join(__dirname, '..', productToUpdate.photo);
              if (fs.existsSync(oldPhotoPath)) {
                fs.unlinkSync(oldPhotoPath);
              }
            }
            req.body.photo = `/uploads/${req.file.filename}`;
          } else {
            // Si no hay imagen nueva, elimina la propiedad 'photo' del req.body para que no sobrescriba la existente en la base de datos
            delete req.body.photo;
          }
    
          await Product.findByIdAndUpdate(req.params.id, req.body, { new: true });
    
          res.status(200).json("Product updated successfully!");
        } catch (error) {
          console.error(error);
          res.status(500).json({message: "Failed to update the product", error: error.message});
        }
    },    

    deleteProduct: async(req, res) => {
        try {
            const productToDelete = await Product.findById(req.params.id);
            if (!productToDelete) {
                return res.status(404).json("Product not found");
            }
            
            // Eliminar la imagen si existe
            if (productToDelete.photo) {
                const photoPath = path.join(__dirname, '..', productToDelete.photo);
                if (fs.existsSync(photoPath)) {
                    fs.unlinkSync(photoPath);
                }
            }
            
            await Product.findByIdAndDelete(req.params.id);
            res.status(200).json("Product deleted successfully!");
        } catch (error) {
            console.error(error);
            res.status(500).json("Failed to delete the product");
        }
    },

    getAllProducts: async(req, res) => {
        try {
            const product = await Product.find().sort({ createdAt: -1 })
            res.status(200).json(product)
        } 
        catch (error) {
            res.status(500).json("Failed to get the products")
        }
    },

    getProduct: async(req, res) => {
        try {
            const product = await Product.findById(req.params.id)
            res.status(200).json(product)
        } 
        catch (error) {     
            res.status(500).json("Failed to get the product")
        }
    },

    searchProduct: async(req, res) => {
        try {
            const result = await Product.aggregate(
                [
                    {
                        $search: {
                            index: "default",
                            text: {
                                query: req.params.key,
                                path: {
                                    wildcard: "*"
                                }
                            }
                        }
                    }
                ]
            );
            res.status(200).json(result);
        } 
        catch (error) {
            res.status(500).json("Failed to get the products");
        }
    }
}