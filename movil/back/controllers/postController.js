const Post = require('../models/Post');
const User = require('../models/User');
const Comment = require('../models/Comment');

// POSTS
exports.getAllPosts = async (req, res) => {
    try {
        let posts = await Post.find().populate('user', 'name email isAdmin'); // Aquí 'name' es el campo que deseas obtener de la colección User.

        posts = posts.map(post => {
            const commentCount = post.comments.length;
            return {...post._doc, commentCount};
        });

        res.status(200).json(posts);
    } catch (err) {
        res.status(500).json({ message: 'Error fetching posts' });
    }
};

exports.getPostById = async (req, res) => {
    try {
        let post = await Post.findById(req.params.postId).populate('user', 'name email isAdmin'); // Aquí 'name' es el campo que deseas obtener de la colección User.

        res.status(200).json(post);
    } catch (err) {
        res.status(500).json({ message: 'Error fetching post' });
    }
};


exports.createPost = async (req, res) => {
    //console.log("User in createPost:", req.user.id);
    try {
        const newPost = new Post({
            content: req.body.content,
            user: req.user.id
        });
        const post = await newPost.save();
        res.status(201).json(post);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Error creating post' });
    }
};

// LIKES
exports.toggleLikePost = async (req, res) => {
    try {
        const postId = req.params.postId;
        const userId = req.user.id;

        const post = await Post.findById(postId).populate('user', 'name email isAdmin');

        if (!post) {
            return res.status(404).json({ message: 'Post not found' });
        }

        if (post.likes.includes(userId)) {
            await Post.findByIdAndUpdate(postId, { $pull: { likes: userId } });
        } else {
            await Post.findByIdAndUpdate(postId, { $addToSet: { likes: userId } });
        }

        const updatedPost = await Post.findById(postId).populate('user', 'name email isAdmin');
        res.status(200).json({ success: true, data: updatedPost });
    } catch (err) {
        res.status(500).json({ message: 'Error toggling like', error: err.message });
    }
};

exports.getUsersByIds = async (req, res) => {
    try {
        const users = await User.find({ _id: { $in: req.body.userIds }});
        res.status(200).json({ success: true, data: users });
    } catch (err) {
        res.status(500).json({ message: 'Error fetching users', error: err.message });
    }
};

// COMMENTS
exports.createComment = async (req, res) => {
    try {
        const newComment = new Comment({
            content: req.body.content,
            user: req.user.id,
            post: req.params.postId,
        });
        const comment = await newComment.save();

        // Actualizar post para incluir el nuevo comentario
        const post = await Post.findById(req.params.postId);
        post.comments.push(comment.id);
        await post.save();

        // Buscar el usuario basado en el ID y añadirlo al comentario
        const user = await User.findById(req.user.id);
        const commentWithUser = {
            ...comment._doc,
            user: {
                name: user.name,
                email: user.email,
                isAdmin: user.isAdmin,
            },
        };
        res.status(201).json(commentWithUser);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Error creating comment' });
    }
};



exports.getPostComments = async (req, res) => {
    try {
        let comments = await Comment.find({post: req.params.postId}).populate('user', 'name email isAdmin');
        res.status(200).json(comments);
    } catch (err) {
        res.status(500).json({ message: 'Error fetching comments' });
    }
};




/*
exports.addLikeToPost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.postId);
        if (!post.likes.includes(req.user.id)) {
            post.likes.push(req.user.id);
            await post.save();
        }
        res.status(200).json(post);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Error adding like' });
    }
};

exports.removeLikeFromPost = async (req, res) => {
    try {
        const post = await Post.findById(req.params.postId);
        const index = post.likes.indexOf(req.user.id);
        if (index !== -1) {
            post.likes.splice(index, 1);
            await post.save();
        }
        res.status(200).json(post);
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Error removing like' });
    }
};
*/