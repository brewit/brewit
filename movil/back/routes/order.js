const express = require('express');
const User = require('../models/User');
const router = express.Router();
const authenticateJWT = require('../auth');

// Crear una nueva orden
router.post('/', authenticateJWT, async (req, res) => {
    const { date, cardLastFour, status, products, total } = req.body;
    console.log(req.body)
    const userId = req.user.id; // Si estás utilizando authenticateJWT, el id del usuario debe estar disponible aquí

    const newOrder = {
        date,
        cardLastFour,
        status,
        products,
        total
    };

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({message: "User not found"});
        }

        // Añadir la nueva orden a las órdenes del usuario
        user.orders.push(newOrder);

        // Vaciar el carrito del usuario
        user.cart = [];

        await user.save();

        res.status(201).json(user.orders[user.orders.length - 1]); // Devuelve la orden recién creada
    } catch (err) {
        res.status(500).json(err);
    }
});

// Obtener todas las órdenes de un usuario específico
router.get('/', authenticateJWT, async (req, res) => {
    try {
        const user = await User.findById(req.user.id);
        res.status(200).json(user.orders);
    } catch (err) {
        res.status(500).json(err);
    }
});

// Actualizar el estado de una orden específica
router.put('/:orderId', authenticateJWT, async (req, res) => {
    const { status } = req.body;

    try {
        const user = await User.findById(req.user.id);
        const order = user.orders.id(req.params.orderId);
        
        if (!order) {
            return res.status(404).json({message: "Order not found"});
        }

        order.status = status;

        await user.save();

        res.status(200).json(order);
    } catch (err) {
        res.status(500).json(err);
    }
});

// Eliminar una orden específica
router.delete('/:orderId', authenticateJWT, async (req, res) => {
    try {
        const user = await User.findById(req.user.id);
        const order = user.orders.id(req.params.orderId);

        if (!order) {
            return res.status(404).json({message: "Order not found"});
        }

        order.remove();

        await user.save();

        res.status(200).json('Orden eliminada.');
    } catch (err) {
        res.status(500).json(err);
    }
});

module.exports = router;
