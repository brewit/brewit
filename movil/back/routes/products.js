const router = require('express').Router();
const multer = require('multer');
const path = require('path');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname))
    }
});
const upload = multer({ storage: storage });
const productController = require('../controllers/productsController');

const uploadOptionally = (req, res, next) => {
    upload.single('photo')(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(400).json({message: "Multer error occurred when uploading.", error: err.message});
        } else if (err) {
            return res.status(400).json({message: "An error occurred when uploading.", error: err.message});
        }
        next(); // Si todo va bien, pasa al siguiente middleware
    });
};


router.get('/', productController.getAllProducts);
router.get('/:id', productController.getProduct);
router.get('/search/:key', productController.searchProduct);
router.post('/', upload.single('photo'), productController.createProduct);
router.put('/:id', upload.single('photo'), productController.updateProduct);
router.put('/:id', uploadOptionally, productController.updateProduct);
router.delete('/:id', productController.deleteProduct);

module.exports = router;
