const express = require('express');
const router = express.Router();
const authenticateJWT = require('../auth');
const postController = require('../controllers/postController');

router.get('/', authenticateJWT, postController.getAllPosts);
router.post('/', authenticateJWT, postController.createPost);
router.get('/:postId', authenticateJWT, postController.getPostById);
router.put('/:postId/toggle-like', authenticateJWT, postController.toggleLikePost);
router.post('/users/bulk-get', authenticateJWT, postController.getUsersByIds);
router.post('/:postId/comments', authenticateJWT, postController.createComment);
router.get('/:postId/comments', authenticateJWT, postController.getPostComments);

module.exports = router;


/*
const express = require('express');
const router = express.Router();
const authenticateJWT = require('../auth');
const postController = require('../controllers/postController');

router.get('/', authenticateJWT, postController.getAllPosts);
router.post('/', authenticateJWT, postController.createPost);
router.post('/:postId/like', authenticateJWT, postController.addLikeToPost);
router.post('/:postId/unlike', authenticateJWT, postController.removeLikeFromPost);

module.exports = router;
*/