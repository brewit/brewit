
require('dotenv').config();
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticateJWT = require('../auth');
const secretKey = process.env.SECRET_KEY;

// mongoDB user model
const User = require('../models/User');

// password handler
const bcrypt = require('bcrypt');

// Función para generar un token JWT
const generateToken = (userId) => {
    return jwt.sign({ id: userId }, secretKey, { expiresIn: '1d' });
};

// Signup
router.post('/signup', (req, res) => {
    let {name, email, _id, password, birthdate} = req.body;
    name = name.trim();
    email = email.trim();
    _id = _id.trim();
    password = password.trim();
    birthdate = birthdate.trim();

    if (name == "" || email == "" || _id == "" || password == "" || birthdate == "") {
        res.json({
            status: "FAILED",
            message: "Empty input fields!"
        });
    } else if (!/^[a-zA-Z ]*$/.test(name)) {
        res.json({
            status: "FAILED",
            message: "Invalid name entered"
        });
    } else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
        res.json({
            status: "FAILED",
            message: "Invalid email entered"
        });
    } else if (!/^\d{10}$/.test(_id)) {
        res.json({
            status: "FAILED",
            message: "Phone number should have exactly 10 digits"
        });
    } else if (!new Date(birthdate).getTime()) {
        res.json({
            status: "FAILED",
            message: "Invalid birthdate entered"
        });
    } else if (password.length < 8) {
        res.json({
            status: "FAILED",
            message: "Password is too short!"
        });
    } else {
        // checking if user already exists
        User.find({email}).then(result => {
            if (result.length) {
                // an user already exists
                res.json({
                    status: "FAILED",
                    message: "User with the provided email already exists"
                });
            } else {
                // try to create a new user

                // password handling
                const saltRounds = 10;
                bcrypt.hash(password, saltRounds).then((hashedPassword) => {
                    const newUser = new User({
                        name,
                        email,
                        _id,
                        password: hashedPassword,
                        birthdate,
                        verified: false
                    });
                    newUser.save().then(result => {
                        // handle account verification
                        //sendVerificationEmail(result, res);
                        const token = generateToken(result._id);
                        res.json({
                            status: "SUCCESS",
                            message: "Signup successful!",
                            data: result,
                            token,
                        });
                    }).catch(err => {
                        console.log(err);
                        res.json({
                            status: "FAILED",
                            message: "An error occured while saving user account"
                        });
                    });
                }).catch(err => {
                    res.json({
                        status: "FAILED",
                        message: "An error occured while hashing password"
                    });
                });
            }
        }).catch(err => {
            console.log(err);
            res.json({
                status: "FAILED",
                message: "An error occured while checking for existing user"
            });
        });
    }
});

// Signin
router.post('/signin', (req, res) => {
    let {email, password} = req.body;
    email = email.trim();
    password = password.trim();

    if (email == "" || password == ""){
        res.json({
            status: "FAILED",
            message: "Empty credentials supplied"
        })
    }
    else {
        // check if user exists
        User.find({email})
        .then(data => {
            if (data.length){
                // user exists
                const hashedPassword = data[0].password;
                bcrypt.compare(password, hashedPassword).then(result => {
                    if (result){
                        // password match
                        const token = generateToken(data[0]._id);
                        res.json({
                            status: "SUCCESS",
                            message: "Signin Successful!",
                            data: data[0],
                            token,
                        });
                    }
                    else {
                        res.json({
                            status: "FAILED",
                            message: "Invalid password entered"
                        })
                    }
                })
                .catch(err => {
                    res.json({
                        status: "FAILED",
                        message: "An error occured while comparing passwords"
                    })
                })
            }
            else {
                res.json({
                    status: "FAILED",
                    message: "Invalid credentials entered"
                })
            }
        })
        .catch(err => {
            res.json({
                status: "FAILED",
                message: "An error occured while checking for existing user"
            })
        })
    }
});

// GET USERS
router.get('/', authenticateJWT, async (req, res) => {
    try {
        const users = await User.find({});
        res.status(200).json(users);
    } catch (err) {
        res.status(500).json(err);
    }
});

// GET USER BY ID
router.get('/:userId', authenticateJWT, async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        if (!user) {
            return res.status(404).json({ message: "User not found." });
        }
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json({ message: "Failed to get user." });
    }
});


// UPDATE USER
router.put('/:userId', authenticateJWT, async (req, res) => {
    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.userId, req.body, { new: true });
        res.status(200).json(updatedUser);
    } catch (err) {
        res.status(500).json({ message: "Failed to update user." });
    }
});


// DELETE USER
router.delete('/:userId', authenticateJWT, async (req, res) => {
    try {
        await User.findByIdAndRemove(req.params.userId);
        res.status(200).json({ message: "User deleted successfully." });
    } catch (err) {
        res.status(500).json({ message: "Failed to delete user." });
    }
});


/*
// Función para actualizar el campo isAdmin
const makeUserAdmin = async (id) => {
    try {
        const user = await User.findByIdAndUpdate(id, { isAdmin: true }, { new: true });
        if (!user) {
            console.log('User not found');
            return;
        }
        console.log('User updated:', user);
    } catch (err) {
        console.error('Error updating user:', err);
    }
};

// Llama a la función para hacer administrador al usuario con _id 6649999999
makeUserAdmin('6649876543');
*/

module.exports = router;