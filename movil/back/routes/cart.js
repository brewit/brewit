const express = require('express');
const User = require('../models/User');
const router = express.Router();
const authenticateJWT = require('../auth');

// Agregar un producto al carrito
router.post('/', authenticateJWT, async (req, res) => {
    const { userId, productId, name, origin, price, photo, quantity } = req.body;
    
    const newCartItem = {
        productId,
        name,
        origin,
        price,
        photo,
        quantity
    };

    try {
        const user = await User.findById(userId);
        user.cart.push(newCartItem);
        await user.save();
        res.status(200).json(newCartItem);
    } catch (err) {
        console.error(err);
        res.status(500).json(err);
    }
});

// Obtener todos los productos del carrito de un usuario específico
/*
router.get('/:userId', authenticateJWT, async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        res.status(200).json(user.cart);
    } catch (err) {
        res.status(500).json(err);
    }
});
*/

// Obtener todos los productos del carrito de un usuario específico
router.get('/', authenticateJWT, async (req, res) => {
    try {
        const user = await User.findById(req.user.id); // Usa el id del usuario que está en req.user
        res.status(200).json(user.cart);
    } catch (err) {
        res.status(500).json(err);
    }
});

// Actualizar la cantidad de un producto específico en el carrito
router.put('/:cartItemId', authenticateJWT, async (req, res) => {
    const { quantity } = req.body;

    try {
        const updatedCartItem = await Cart.findByIdAndUpdate(
            req.params.cartItemId,
            { quantity },
            { new: true }
        );
        res.status(200).json(updatedCartItem);
    } catch (err) {
        res.status(500).json(err);
    }
});

// Eliminar un producto específico del carrito
router.delete('/:cartItemId', authenticateJWT, async (req, res) => {
    try {
        const user = await User.findById(req.user.id); // Obten el usuario que está realizando la acción
        const updatedCart = user.cart.filter(item => item._id.toString() !== req.params.cartItemId); // Filtra el carrito para eliminar el item deseado
        user.cart = updatedCart; // Actualiza el carrito del usuario con el nuevo array
        await user.save(); // Guarda el cambio en la base de datos
        res.status(200).json('Producto eliminado del carrito.');
    } catch (err) {
        res.status(500).json(err);
    }
});

module.exports = router;
