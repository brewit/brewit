import React, { useContext, useCallback } from 'react';
import { Colors } from '../components/styles';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { CredentialsContext } from '../components/CredentialsContext';
import { Ionicons } from "@expo/vector-icons";
import { SafeAreaView, StyleSheet } from 'react-native';
import * as SplashScreen from "expo-splash-screen";
import { useFonts } from 'expo-font';
import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Home from '../screens/Home';
import Search from '../screens/Search';
import Community from '../screens/Community';
import Profile from '../screens/Profile';
import ProductDetails from '../screens/ProductDetails';
import NewRivals from '../screens/NewRivals';
import Cart from '../components/cart/Cart';
import Favorites from '../screens/Favorites';
import Payment from '../components/cart/payment/Payment';
import Order from '../components/order/Order';
import OrderDetails from '../components/order/OrderDetails';
import Admin from '../screens/admin/Admin';
import Users from '../screens/admin/Users';
import Products from '../screens/admin/Products';
import WelcomePage from '../screens/WelcomePage';
import NewPost from '../components/post/NewPost';
import PostDetails from '../components/post/PostDetails';
import Dashboard from '../components/IoT/dashboard';
import LineChartComponent from '../components/IoT/LineChart';
import MQTT_Form from '../components/IoT/MQTT_Form';
import Datos from '../components/IoT/Datos';

const { primary, tertiary } = Colors;
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStack = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Stack.Navigator
        screenOptions={{
          headerStyled: {
            backgroundColor: 'transparent',
          },
          headerTintColor: tertiary,
          headerTransparent: true,
          headerTitle: '',
          headerLeftContainerStyle: {
            paddingLeft: 20,
          },
        }}
      >
        <Stack.Screen name="Home" component={Home} />
      </Stack.Navigator>
    </SafeAreaView>
  );
};

const MainStack = () => {
  const { storedCredentials } = useContext(CredentialsContext);


  return(
    <SafeAreaView style={styles.container}>
      <Tab.Navigator
              screenOptions={({ navigation }) => ({
                tabBarShowLabel: false,
                tabBarHideOnKeyboard: true,
                headerShown: false,
                tabBarStyle: {
                  position: 'absolute',
                  bottom: 0,
                  right: 0,
                  left: 0,
                  elevation: 0,
                  height: 70,
                },
                tabBar: () => <BottomNavBar navigation={navigation} />,
              })}
              initialRouteName="Profile"
            >
              <Tab.Screen 
                name="HomeStack" 
                component={HomeStack}  // Usar el nuevo StackNavigator para la pantalla "Home"
                options={{
                  tabBarIcon: ({ focused }) => {
                    return (
                      <Ionicons 
                        name={focused ? "home" : "home-outline"} 
                        size={24} 
                        color={focused ? Colors.brand : Colors.darkLight}
                      />
                    )
                  }
                }}
              />

              <Tab.Screen 
                name="Search" 
                component={Search} 
                options={{
                  tabBarIcon: ({ focused }) => {
                    return (
                      <Ionicons 
                        name={focused ? "search-sharp" : "search-sharp"} 
                        size={24} 
                        color={focused ? Colors.brand : Colors.darkLight}
                      />
                    )
                  }
                }}
              />

              {storedCredentials.isAdmin && (
                <Tab.Screen 
                  name="Admin" 
                  component={Admin} 
                  options={{
                    tabBarIcon: ({ focused }) => {
                      return (
                        <Ionicons 
                          name={focused ? "server" : "server-outline"} 
                          size={24} 
                          color={focused ? Colors.brand : Colors.darkLight}
                        />
                      )
                    }
                  }}
                />
              )}

              <Tab.Screen 
                name="Community" 
                component={Community} 
                options={{
                  tabBarIcon: ({ focused }) => {
                    return (
                      <Ionicons 
                        name={focused ? "newspaper" : "newspaper-outline"}
                        size={24} 
                        color={focused ? Colors.brand : Colors.darkLight}
                      />
                    )
                  }
                }}
              />

              <Tab.Screen 
                name="Profile" 
                component={Profile} 
                options={{
                  tabBarIcon: ({ focused }) => {
                    return (
                      <Ionicons 
                        name={focused ? "person" : "person-outline"} 
                        size={24} 
                        color={focused ? Colors.brand : Colors.darkLight}
                      />
                    )
                  }
                }}
              />
      </Tab.Navigator>
    </SafeAreaView>
  )
}


const RootStack = () => {

  const [fontsLoaded] = useFonts({
    regular: require("../assets/fonts/Poppins-Regular.ttf"),
    light: require("../assets/fonts/Poppins-Light.ttf"),
    bold: require("../assets/fonts/Poppins-Bold.ttf"),
    medium: require("../assets/fonts/Poppins-Medium.ttf"),
    extrabold: require("../assets/fonts/Poppins-ExtraBold.ttf"),
    semibold: require("../assets/fonts/Poppins-SemiBold.ttf")
  });

  const onLayoutRootView = useCallback(async() => {
      if(fontsLoaded){
        await SplashScreen.hideAsync();
      }
  }, [fontsLoaded]);

  if(!fontsLoaded){
    return null;
  }

  return (
    <CredentialsContext.Consumer>
      {({ storedCredentials }) => (
        <NavigationContainer>
          {storedCredentials ? (
            <SafeAreaView style={styles.container}>
              <Stack.Navigator
                screenOptions={{
                  headerStyled: {
                    backgroundColor: 'transparent',
                  },
                  headerTintColor: tertiary,
                  headerTransparent: true,
                  headerTitle: '',
                  headerLeftContainerStyle: {
                    paddingLeft: 20,
                  },
                  headerBackVisible: false
                }}
              >
                <Stack.Screen name="MainStack" component={MainStack} />
                <Stack.Screen name="ProductDetails" component={ProductDetails} />
                <Stack.Screen name="ProductList" component={NewRivals} />
                <Stack.Screen name="Cart" component={Cart} />
                <Stack.Screen name="Favorites" component={Favorites} />
                <Stack.Screen name="Payment" component={Payment} />
                <Stack.Screen name="Order" component={Order} />
                <Stack.Screen name="OrderDetails" component={OrderDetails} />
                <Stack.Screen name="Users" component={Users} />
                <Stack.Screen name="Products" component={Products} />
                <Stack.Screen name="NewPost" component={NewPost} />
                <Stack.Screen name="PostDetails" component={PostDetails} />
                <Stack.Screen name="Dashboard" component={Dashboard} />
                <Stack.Screen name="LineChart" component={LineChartComponent} />
                <Stack.Screen name="MQTT_Form" component={MQTT_Form} />
                <Stack.Screen name="Datos" component={Datos} />
              </Stack.Navigator>
            </SafeAreaView>
          ) : (
            <SafeAreaView style={styles.container}>
              <Stack.Navigator
                screenOptions={{
                  headerStyled: {
                    backgroundColor: 'transparent',
                  },
                  headerTintColor: tertiary,
                  headerTransparent: true,
                  headerTitle: '',
                  headerLeftContainerStyle: {
                    paddingLeft: 20,
                  },
                }}
                initialRouteName="WelcomePage"
              >
                <Stack.Screen name="WelcomePage" component={WelcomePage} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Signup" component={Signup} />
              </Stack.Navigator>
            </SafeAreaView>
          )}
        </NavigationContainer>
      )}
    </CredentialsContext.Consumer>
  );
};

export default RootStack;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: 20
  }
});





/*
// RootStack.js
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { Colors } from '../components/styles';
import Welcome from '../screens/Welcome';
import Search from '../screens/Search';
import Community from '../screens/Community';
import Profile from '../screens/Profile';
import BottomNavBar from '../components/BottomNavBar';

const { brand, tertiary } = Colors;
const Tab = createBottomTabNavigator();

const RootStack = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ navigation }) => ({
          tabBarShowLabel: false,
          tabBarHideOnKeyboard: true,
          headerShown: false,
          tabBarStyle: {
            position: 'absolute',
            bottom: 0,
            right: 0,
            left: 0,
            elevation: 0,
            height: 70,
          },
          // Pasa la navegación como prop al componente BottomNavBar
          tabBar: () => <BottomNavBar navigation={navigation} />,
        })}
        initialRouteName="Welcome"
      >
        <Tab.Screen name="Welcome" component={Welcome} />
        <Tab.Screen name="Search" component={Search} />
        <Tab.Screen name="Community" component={Community} />
        <Tab.Screen name="Profile" component={Profile} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default RootStack;
*/


/*
import React from 'react';
import { Colors } from '../components/styles';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { CredentialsContext } from '../components/CredentialsContext';
import { Login, Signup, Welcome, Search, Community, Profile } from '../screens/Screens';
import Verification from '../screens/LinkVerification';
import BottomNavBar from '../components/BottomNavBar';

const {primary, tertiary} = Colors;
const Stack = createNativeStackNavigator();

const RootStack = () => {
    return(
        <CredentialsContext.Consumer>
            {({storedCredentials}) => (
                <NavigationContainer>
                    <Stack.Navigator
                        screenOptions={{
                            headerStyled: {
                                backgroundColor: "transparent"
                            },
                            headerTintColor: tertiary,
                            headerTransparent: true,
                            headerTitle: '',
                            headerLeftContainerStyle: {
                                paddingLeft: 20
                            }
                        }}
                        initialRouteName="Login"
                    >

                    {storedCredentials ? (
                        <>
                            <Stack.Screen options={{headerTintColor: primary}} name="Welcome" component={Welcome} />
                        </>
                    )   :   (
                        <>
                            {/* <Stack.Screen name="Verification" component={Verification} /> *//*}
                            // <Stack.Screen name="Login" component={Login} />
                            // <Stack.Screen name="Signup" component={Signup} />
                            
                        </>
                    )}

                    </Stack.Navigator>
                </NavigationContainer>
            )}
        </CredentialsContext.Consumer>
    );
}

export default RootStack;
*/