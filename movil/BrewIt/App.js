import React, { useState, useEffect } from 'react';
import RootStack from './navigators/RootStack';
import * as SplashScreen from 'expo-splash-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CredentialsContext } from './components/CredentialsContext';
import { CartProvider } from './components/cart/CartContext';
import { UserProvider } from './components/auth/UserContext';
import Toast from 'react-native-toast-message';
// ignore ViewPropType Warnings, UPDATE TO PropTypes (TypeScript) LATER
import { LogBox } from 'react-native';
LogBox.ignoreLogs(['ViewPropTypes will be removed']);
LogBox.ignoreLogs(['Warning: componentWillReceiveProps']);
LogBox.ignoreLogs(['Warning: Non-serializable values']);
LogBox.ignoreLogs(['Non-serializable values were found in the navigation state']);


export default function App() {
  const [storedCredentials, setStoredCredentials] = useState(null);

  useEffect(() => {
    prepareApp();
  }, []);

  const prepareApp = async () => {
    try {
      await SplashScreen.preventAutoHideAsync();
      await checkLoginCredentials();
      await SplashScreen.hideAsync();
    } 
    catch (error) {
      console.log(error);
    }
  };

  const checkLoginCredentials = async () => {
    try {
      const result = await AsyncStorage.getItem('brewItCredentials');
      if (result !== null) {
        setStoredCredentials(JSON.parse(result));
      } 
      else {
        setStoredCredentials(null);
      }
    } 
    catch (error) {
      console.log(error);
    }
  };

  return (
    <CredentialsContext.Provider value={{ storedCredentials, setStoredCredentials }}>
      <CartProvider>
        <UserProvider>
          <RootStack />
        </UserProvider>
      </CartProvider>
      <Toast />
    </CredentialsContext.Provider>
  );
}
