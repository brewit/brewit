import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    container: {
        width: 182,
        height: 240,
        marginEnd: 22,
        borderRadius: SIZES.medium,
        backgroundColor: COLORS.secondary
    },
    imageContainer: {
        flex: 1,
        width: 169,
        marginLeft: SIZES.small / 2,
        marginTop: SIZES.small / 2,
        borderRadius: SIZES.small,
        overflow: "hidden",
    },
    image: {
        flex: 1,
        width: "100%",
        resizeMode: "cover"
    },
    details: {
        padding: SIZES.small
    },
    title: {
        fontFamily: "bold",
        fontSize: SIZES.large-2,
        marginBottom: 2,
        marginLeft: -2
    },
    supplier: {
        fontFamily: "regular",
        fontSize: SIZES.small+1,
        color: COLORS.gray,
        marginLeft: -1
    },
    price: {
        //fontFamily: "bold"
        fontWeight: "bold",
        fontSize: SIZES.medium,
        marginLeft: -2
    },
    addBtn: {
        position: "absolute",
        bottom: SIZES.xSmall,
        right: SIZES.xSmall
    }
});

export default styles;