import { TouchableOpacity, Text, View, Image } from 'react-native';
import React from 'react';
import styles from './searchTileStyle';
import baseURL from "../../env";
import { useNavigation } from '@react-navigation/native';

const SearchTile = ({item}) => {
    const navigation = useNavigation();

    const getImageUri = (path) => {
        if(path.startsWith('/uploads/')) {
            return `${baseURL}${path}`;
        }
        return path;
    }


    return (
        <View>
            <TouchableOpacity 
                style={styles.container} 
                onPress={() => navigation.navigate("ProductDetails", {item})}
            >
                <View style={styles.image}>
                    <Image 
                        source={{uri: getImageUri(item.photo)}} 
                        style={styles.productImg}
                    />
                </View>

                <View style={styles.textContainer}>
                    <Text style={styles.productTitle}>{item.name}</Text>
                    <Text style={styles.supplier}>{item.origin}</Text>
                    <Text style={styles.supplier}>${item.price}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default SearchTile;