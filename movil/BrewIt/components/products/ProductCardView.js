import { TouchableOpacity, Text, View, Image, Alert } from 'react-native';
import React, { useContext } from 'react';
import styles from './productCardViewStyle';
import { Ionicons } from "@expo/vector-icons"
import { COLORS } from '../../constants';
import { useNavigation } from '@react-navigation/native';
import { CredentialsContext } from '../../components/CredentialsContext';
import { useCart } from '../cart/CartContext';
import Toast from 'react-native-toast-message';
import axios from 'axios';
import baseURL from '../../env';

const ProductCardView = ({item}) => {
    const navigation = useNavigation();
    const { storedCredentials } = useContext(CredentialsContext);
    const { cartItems, setCartItems } = useCart();

    const handleBuyOneNow = async () => {
        const uniqueId = Date.now();
        const count = 1;
        const itemWithQuantity = { ...item, quantity: count, id: uniqueId };
        setCartItems([...cartItems, itemWithQuantity]);

        try {
            const response = await axios.post(`${baseURL}/api/cart`, {
              userId: storedCredentials._id,
              productId: uniqueId,
              name: item.name,
              origin: item.origin,
              price: item.price,
              photo: item.photo,
              quantity: count
          }, {
              headers: {
                  Authorization: `Bearer ${storedCredentials.token}`
              }
        });
            // Alert.alert("Alert", "Product added to Cart successfully!");
            Toast.show({
              type: 'success',
              position: 'bottom',
              text1: 'Product added to Cart successfully!',
              visibilityTime: 3000,
              autoHide: true,
              topOffset: 30,
              bottomOffset: 40,
              text1Style: {
                fontSize: 20,
              },
            });
            console.log(response.data);
        } 
        catch (err) {
            Alert.alert("Error", err.message || "An error occurred");
            console.log(err);
        }
    };

    const getImageUri = (path) => {
        if(path.startsWith('/uploads/')) {
            return `${baseURL}${path}`;
        }
        return path;
    }
    


    return (
        <TouchableOpacity onPress={() => navigation.navigate("ProductDetails", {item})}>
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image 
                        source={{ uri: getImageUri(item.photo) }}
                        style={styles.image}
                    />
                </View>

                <View style={styles.details}>
                    <Text style={styles.title} numberOfLines={1}> {item.name} </Text>
                    <Text style={styles.supplier} numberOfLines={1}> {item.origin} </Text>
                    <Text style={styles.price}> ${item.price} </Text>
                </View>
                <TouchableOpacity style={styles.addBtn} onPress={handleBuyOneNow}>
                    <Ionicons name="add-circle" size={35} color={COLORS.primary} />
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}

export default ProductCardView;