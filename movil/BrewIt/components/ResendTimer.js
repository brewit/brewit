import React from "react";
import { ActivityIndicator, View } from "react-native";
import { 
    InfoText, 
    EmphasizeText, 
    InLineGroup, 
    TextLink, 
    TextLinkContent,
    Colors
} from '../components/styles';

const { brand } = Colors;

const ResendTimer = ({ activeResend, resendEmail, resendingEmail, resendStatus, timeLeft, targetTime }) => {
    return(
        <View>
            <InLineGroup>
                <InfoText> Didn't receive de email? </InfoText>

                {!resendingEmail && (
                    <TextLink 
                        style={{ opacity: activeResend ? 1 : 0.5 }}
                        disabled={!activeResend} 
                        onPress={resendEmail}
                    >
                        <TextLinkContent 
                            resendStatus={resendStatus}
                            style={{ textDecorationLine: 'underline' }}
                        >
                            {resendStatus}
                        </TextLinkContent>
                    </TextLink>
                )}

                {resendingEmail && (
                    <TextLink 
                        disabled
                    >
                        <TextLinkContent>
                            <ActivityIndicator color={brand} />
                        </TextLinkContent>
                    </TextLink>
                )}

            </InLineGroup>
            {!activeResend && (
                <InfoText>
                    in <EmphasizeText>{timeLeft || targetTime}</EmphasizeText> second(s)
                </InfoText>
            )}
        </View>
    )
}

export default ResendTimer;