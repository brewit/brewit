import React, { useEffect, useState, useContext } from 'react';
import { Text, View, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons } from "@expo/vector-icons";
import styles from './cartStyle';
import { useNavigation } from '@react-navigation/native';
import { useCart } from './CartContext';
import CartTile from './CartTile';
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';

const Cart = () => {
  const navigation = useNavigation();
  const { cartItems, setCartItems } = useCart(); // Hacer uso de `setCartItems` aquí
  const { storedCredentials } = useContext(CredentialsContext);
  const [token, setToken] = useState(null);

  useEffect(() => {
    if (storedCredentials && storedCredentials.token) {
      setToken(storedCredentials.token);
    }
  }, [storedCredentials]);

  const fetchCartItems = async () => {
    if (!storedCredentials.token) return;
  
    try {
      const response = await axios.get(`${baseURL}/api/cart`, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
        },
      });
      setCartItems(response.data); // Actualizar los elementos del carrito aquí
    } catch (e) {
      console.log('Failed to fetch cart items', e);
    }
  };

  useEffect(() => {
    fetchCartItems();
  }, [storedCredentials]);

  const calculateSubtotal = () => {
    let subtotal = 0;
    for (let item of cartItems) {
      subtotal += item.price * item.quantity;
    }
    return subtotal;
  };

  const calculateTotal = (subtotal) => {
    // Aquí podrías agregar cualquier lógica adicional para calcular
    // impuestos, cargos por envío, etc. Por ahora, solo devolvemos el subtotal.
    return subtotal;
  };

  // Aquí es donde llamamos a las funciones para calcular los totales
  const subtotal = calculateSubtotal();
  const total = calculateTotal(subtotal);

  return (
    <SafeAreaView style={styles.cartContainer}>
      <View style={styles.upperRow}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="chevron-back-circle" size={30} />
        </TouchableOpacity>
        <Text style={styles.cartText}> Cart </Text>
      </View>

      <View style={{ flex: 1, marginTop: 50 }}>
        <FlatList
          data={cartItems}
          renderItem={({ item }) => <CartTile item={item} fetchCartItems={fetchCartItems} />}
          keyExtractor={(item) => item._id}
          style={{ marginHorizontal: 12 }}
        />
        

        <View style={styles.orderContainer}>
            <View style={styles.order}>
              <Text style={styles.orderText}>Order Info</Text>
            </View>

            <View style={styles.subtotalContainer}>
              <Text style={styles.subtotalText}>Subtotal</Text>
              <Text style={styles.subtotal}>${subtotal.toFixed(2)}</Text>
            </View>

            <View style={styles.totalContainer}>
              <Text style={styles.totalText}>Total</Text>
              <Text style={styles.total}>${total.toFixed(2)}</Text>
            </View>

            <TouchableOpacity onPress={() => navigation.navigate("Payment", { total: total.toFixed(2) })}>
              <View style={styles.checkoutBtn}>
                <Text style={styles.checkout}>Pay ${total.toFixed(2)}</Text>
              </View>
            </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default Cart;