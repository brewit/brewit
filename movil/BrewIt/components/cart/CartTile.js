import { Text, View, Image, TouchableOpacity, Alert } from 'react-native';
import React, { useContext } from 'react';
import { Ionicons } from "@expo/vector-icons";
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';
import styles from './cartTileStyle';
import Toast from 'react-native-toast-message';
import { COLORS } from '../../constants';


const CartTile = ({ item, fetchCartItems }) => {
  const { storedCredentials } = useContext(CredentialsContext);

  const handleDelete = () => {
    Alert.alert(
      "Delete",
      `¿Are you sure you want to delete "${item.name}" from your Cart?`,
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { 
          text: "Yes", 
          onPress: async () => {
            try {
              const response = await axios.delete(`${baseURL}/api/cart/${item._id}`, {
                headers: {
                  Authorization: `Bearer ${storedCredentials.token}`,
                },
              });
              Toast.show({
                type: 'success',
                position: 'bottom',
                text1: 'Product deleted successfully!',
                visibilityTime: 3000,
                autoHide: true,
                topOffset: 30,
                bottomOffset: 40,
                text1Style: {
                  fontSize: 20
                },
              });
              console.log(response.data);
              fetchCartItems();
            } catch (e) {
              console.log('Failed to delete item', e);
            }
          } 
        }
      ]
    );
  };

  const getImageUri = (path) => {
    if(path.startsWith('/uploads/')) {
        return `${baseURL}${path}`;
    }
    return path;
  }


  return (
    <View style={styles.container}>
      <View style={styles.image}>
        <Image 
          source={{uri: getImageUri(item.photo) }} 
          style={styles.productImg}
        />
      </View>
  
      <View style={styles.textContainer}>
        <View style={{flex: 1}}>
          <Text style={styles.productTitle}>{item.name} ({item.quantity}) </Text>
          <Text style={styles.origin}>{item.origin}</Text>
          <Text style={styles.price}>${item.price}</Text>
        </View>
  
        <View>
          <TouchableOpacity onPress={handleDelete} style={styles.trash}>
            <Ionicons name="trash-outline" size={28} color={COLORS.primary} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default CartTile;
