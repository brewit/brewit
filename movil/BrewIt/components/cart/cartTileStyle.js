import { StyleSheet } from "react-native";
import { COLORS, SIZES, SHADOWS } from "../../constants/index";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 10,
        flexDirection: "row",
        padding: SIZES.medium,
        paddingTop: -8,
        paddingBottom: -8,
        borderRadius: SIZES.small+10,
        backgroundColor: "#FFF",
        ...SHADOWS.medium,
        shadowColor: COLORS.lightWhite,
    },
    image: {
        width: 70,
        backgroundColor: COLORS.secondary,
        borderRadius: SIZES.medium,
        justifyContent: "center",
        alignItems: "center"
    },
    productImg: {
        width: "100%",
        height: 65,
        borderRadius: SIZES.small,
        resizeMode: "cover"
    },
    textContainer: {
        flex: 1,
        marginHorizontal: SIZES.medium,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    productTitle: {
        fontSize: SIZES.medium,
        fontFamily: "bold",
        color: COLORS.black,
        marginBottom: -7,
        marginTop: 5
    },
    origin: {
        fontSize: SIZES.small+2,
        fontFamily: "regular",
        color: COLORS.gray,
        marginTop: 3,
    },
    price: {
        fontSize: SIZES.small+2,
        fontFamily: "semibold",
        color: COLORS.gray,
    },
    trash: {
        marginTop: -60
    }
});

export default styles;