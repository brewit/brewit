import { StyleSheet } from "react-native";
import { COLORS, SIZES, SHADOWS } from "../../constants";


const styles = StyleSheet.create({
    cartContainer: {
        flex: 1,
        backgroundColor: COLORS.offwhite,
    },
    upperRow: {
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        top: SIZES.xxLarge,
        width: SIZES.width -44,
        marginTop: -10,
        zIndex: 999
    },
    cartText: {
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary,
        marginTop: 7
    },
    products: {
        marginTop: 110
    },
    orderContainer: {
        backgroundColor: "white",
        padding: 20,
        paddingTop: -15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        // iOS shadow properties
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -5, // A negative value here means the shadow will be cast upwards
        },
        shadowOpacity: 0.9,
        shadowRadius: 30,

        // Android shadow
        elevation: 70,
    },
    order: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginTop: SIZES.xLarge,
        marginLeft: SIZES.large
    },
    orderText: {
        fontFamily: "semibold",
        fontSize: SIZES.medium
    },
    subtotalContainer: {
        marginTop: SIZES.large,
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    subtotalText: {
        fontFamily: "regular",
        color: COLORS.gray
    },
    subtotal: {
        fontFamily: "regular",
        fontSize: SIZES.medium,
        color: COLORS.gray
    },
    totalContainer: {
        marginTop: SIZES.xSmall,
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    totalText: {
        fontFamily: "regular",
        color: COLORS.gray
    },
    total: {
        fontFamily: "bold",
        fontSize: SIZES.large,
        color: COLORS.black
    },
    checkoutBtn: {
        flexDirection: "row",
        marginHorizontal: 24,
        borderRadius: 20,
        backgroundColor: COLORS.primary,
        marginTop: 30,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10,
        marginHorizontal: 60
    },
    checkout: {
        fontFamily: "semibold",
        fontSize: SIZES.small+2,
        marginBottom: -2,
        color: COLORS.lightWhite
    }
});

export default styles;