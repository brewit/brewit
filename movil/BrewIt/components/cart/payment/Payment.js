import React, { useState, useEffect, useContext } from 'react';
import { View, Text, TouchableOpacity, UIManager, Platform, Alert } from 'react-native';
import { CreditCardInput } from 'react-native-credit-card-input';
import { Ionicons } from "@expo/vector-icons";
import { COLORS, SIZES } from '../../../constants';
import styles from './paymentStyle';
import axios from 'axios';
import baseURL from '../../../env';
import { CredentialsContext } from '../../../components/CredentialsContext';
import { useCart } from '../CartContext';

const Payment = (props) => {
    const { storedCredentials } = useContext(CredentialsContext);
    const { cartItems } = useCart();
    const [cardData, setCardData] = useState({});
    const [processingPayment, setProcessingPayment] = useState(false);
    const [total, setTotal] = useState(props.route.params.total);

    useEffect(() => {
      // Habilitar LayoutAnimation en android
      if (Platform.OS === 'android') {
        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
      }
    }, []);

    const handleConfirmPayment = async () => {
      setProcessingPayment(true);
    
      try {
        const paymentResponse = await axios.post(`${baseURL}/api/payment`, {
          card: cardData,
          amount: total
        }, {
          headers: {
            Authorization: `Bearer ${storedCredentials.token}`,
          },
        });
        
        if (paymentResponse.data.success) {
          const orderResponse = await axios.post(`${baseURL}/api/orders`, {
            date: new Date(),
            cardLastFour: cardData.values.number.slice(-4),
            status: "pending",
            products: cartItems,
            total: total,
          }, {
            headers: {
              Authorization: `Bearer ${storedCredentials.token}`,
            },
          });
          Alert.alert("Payment", "Payment successful!");
          props.navigation.navigate("HomeStack");
        } else {
          setProcessingPayment(false);
          alert('El pago no se procesó correctamente. Por favor, inténtalo de nuevo.');
        }
      } catch (err) {
        setProcessingPayment(false);
        alert('Hubo un problema. Por favor, inténtalo de nuevo.');
      }
    };

    return (
      <>
          <View style={styles.upperRow}>
            <TouchableOpacity onPress={() => props.navigation.goBack()}>
              <Ionicons name="chevron-back-circle" size={30} />
            </TouchableOpacity>
            <Text style={styles.cartText}> Payment </Text>
          </View>

          <View style={styles.container}>
            <View style={styles.cardInputContainer}>
              <Text style={styles.title}>Insert your card information</Text>
              <CreditCardInput onChange={(form) => setCardData(form)} />
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity style={styles.confirmButton} onPress={handleConfirmPayment}>
                <Text style={styles.confirmButtonText}>Confirm Payment</Text>
              </TouchableOpacity>
            </View>
          </View>
      </>
    );
}

export default Payment;
