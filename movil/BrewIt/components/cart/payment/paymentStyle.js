import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../../constants";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginTop: 230,
        marginHorizontal: 30
    },
    upperRow: {
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        top: SIZES.xxLarge,
        width: SIZES.width -44,
        marginTop: 10,
        zIndex: 999
    },
    cartText: {
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary,
        marginTop: 7
    },
    cardInputContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginTop: 5
    },
    buttonContainer: {
        flex: 1,
        marginTop: 80,
        justifyContent: 'flex-start',
        marginHorizontal: 62
    },
    title: {
        fontSize: 18,
        marginTop: -70,
        fontFamily: 'medium',
        textAlign: 'center',
        marginBottom: 110,
    },
    confirmButton: {
        backgroundColor: COLORS.primary,
        borderRadius: 20,
        padding: 10,
        alignItems: 'center',
    },
    confirmButtonText: {
        color: 'white',
        fontFamily: "semibold",
        fontSize: 16,
    },
    aboutContainer: {
        position: 'absolute',
        bottom: 10,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 22,
    },
    about: {
        fontFamily: "light",
        fontSize: SIZES.small,
        fontStyle: "italic",
        color: COLORS.gray,
        textAlign: "center",
        marginTop: -100
    }
});

export default styles;