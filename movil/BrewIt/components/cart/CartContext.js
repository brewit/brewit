import React, { createContext, useState, useContext } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const CartContext = createContext();

export function CartProvider({ children }) {
  const [cartItems, setCartItems] = useState([]);
  const [token, setToken] = useState(null);  // Añadir un estado para el token

  const addItemToCart = (item) => {
    setCartItems(prevItems => [...prevItems, item]);
  }

  const removeItemFromCart = (itemId) => {
    setCartItems(prevItems => prevItems.filter(item => item._id !== itemId));
  }

  // Guarda el token del usuario en memoria y en estado
  const storeToken = async (userToken) => {
    try {
      setToken(userToken);
      await AsyncStorage.setItem('userToken', JSON.stringify(userToken));
    } catch (e) {
      console.log('Failed to save the token to storage');
      console.log(e);
    }
};

  // Obtiene el token del usuario de memoria y lo establece en el estado
  const loadToken = async () => {
    try {
      const userToken = await AsyncStorage.getItem('userToken');
      setToken(userToken);
    } catch (e) {
      console.log('Failed to load the token from storage');
    }
  };

  return (
    <CartContext.Provider value={{ cartItems, setCartItems, addItemToCart, removeItemFromCart, token, storeToken, loadToken }}>
      {children}
    </CartContext.Provider>
  );
}

export function useCart() {
  const context = useContext(CartContext);
  if (!context) {
    throw new Error('useCart must be used within a CartProvider');
  }
  return context;
}
