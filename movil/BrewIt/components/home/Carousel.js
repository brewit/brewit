
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
// import { ViewPropTypes } from 'deprecated-react-native-prop-types';
import Carousel from 'react-native-snap-carousel';
import { COLORS } from '../../constants';

const slides = [
  //require('../../assets/img/logo.jpg'),
  require('../../assets/img/brew1.jpg'),
  require('../../assets/img/brew2.jpg'),
  require('../../assets/img/brew3.jpg'),
  require('../../assets/img/brew4.webp'),
  require('../../assets/img/brew5.jpg')
];

const renderItem = ({ item }) => {
  return (
    <View style={styles.carouselItem}>
      <Image source={item} style={styles.carouselImage} />
    </View>
  );
};

const CarouselComponent = () => {
  return (
    <View style={styles.carouselContainer}>
      <Carousel
        data={slides}
        renderItem={renderItem}
        sliderWidth={380}
        itemWidth={270}
        loop={true}
        autoplay={true}
        dotColor={COLORS.primary}
        inactiveDotColor={COLORS.secondary}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  carouselContainer: {
    flex: 1,
    alignItems: 'center',
  },
  carouselItem: {
    width: 250,
    height: 150,
    borderRadius: 15,
    overflow: 'hidden',
    marginHorizontal: 10,
  },
  carouselImage: {
    width: '100%',
    height: '100%',
  },
});

export default CarouselComponent;

