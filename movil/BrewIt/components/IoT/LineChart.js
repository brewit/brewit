import React, { useEffect, useState, useContext } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import baseURL from '../../env';
import { CredentialsContext } from '../CredentialsContext';


const LineChartComponent = (props) => {
  const [seriesData, setSeriesData] = useState([]);
  const [startDateTime, setStartDateTime] = useState('');
  const userId = props.userId;
  const maxDataPoints = 5;
  const { storedCredentials } = useContext(CredentialsContext);

  const fetchData = () => {
    fetch(`${baseURL}/api/users/${userId}`, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
        },
    })//change this to the ip of the machine
      .then(response => response.json())
      .then(data => {
        const lastItem = data.data[data.data.length - 1];
        const processedData = lastItem.ArrayHistory.map(item => ({
          x: new Date(item.DATE_TIME).getTime(),
          y: item.T,
        }));
        setSeriesData(processedData);
        const startDate = new Date(lastItem.StartDateTime);
        setStartDateTime(startDate.toLocaleDateString());
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  };

  useEffect(() => {
    fetchData();

    const interval = setInterval(() => {
      fetchData();
    }, 10000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  const chartConfig = {
    backgroundGradientFrom: '#B73E3E',
    backgroundGradientTo: '#B73E3E',
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: '6',
      strokeWidth: '2',
      stroke: '#F1C93B',
    },
    xLabelsRotation: 90,
  };

  // Filter the data to show only the last `maxDataPoints` points
  const filteredData = seriesData.slice(-maxDataPoints);

  return (
    <View style={styles.container}>
      {seriesData.length > 0 ? (
        <LineChart
          data={{
            labels: filteredData.map(data => new Date(data.x).toLocaleTimeString()),
            datasets: [
              {
                data: filteredData.map(data => data.y),
              },
            ],
          }}
          width={400}
          height={220}
          chartConfig={chartConfig}
          bezier
        />
      ) : (
        <Text>Loading data...</Text>
      )}
      <Text style={styles.chartTitle}>
        Temperature from Brewer - {startDateTime}
      </Text>
      <Text style={styles.chartSubtitle}>BrewIt!</Text>
    </View>
  );
  
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  chartTitle: {
    color: '#F1C93B',
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 10,
  },
  chartSubtitle: {
    color: '#DDD',
  },
});

export default LineChartComponent;
