import React, { useState, useEffect, useContext } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import baseURL from '../../env';
import { CredentialsContext } from '../CredentialsContext';

const Dashboard = (props) => {
  const degreeSymbol = '\u00B0';

  const [duration, setDuration] = useState(null);
  const [dashboardData, setDashboardData] = useState([]);
  const [brewDuration, setBrewDuration] = useState(Date.now() + 100000);
  const { storedCredentials } = useContext(CredentialsContext);
  const userId = props.userId;
 // console.log(duration);

  useEffect(() => {
    // Fetch dashboard data
    const fetchData = async () => {
      try {
        const response = await fetch(`${baseURL}/api/users/${userId}`, {
            headers: {
              Authorization: `Bearer ${storedCredentials.token}`,
            },
        });// the ip is from the machine 
        const userData = await response.json();
        const { data } = userData;

        if (data && data.length > 0) {
          const lastItem = data[data.length - 1];
          const latestHistoricalData = lastItem.ArrayHistory || [];
          const latestData = latestHistoricalData.length > 0 ? latestHistoricalData[latestHistoricalData.length - 1] : {};

          // Update the state with the latest data
          setDashboardData(latestData);

           // Fetch the last duration from the data array
        if (userData && userData.data && userData.data.length > 0) {
          const lastData = userData.data[userData.data.length - 1];
          const lastDuration = lastData.duration;
          // Update the brewing duration
          if (duration !== lastDuration) {
            setDuration(lastDuration);
            // console.log(setDuration)
            setBrewDuration(lastDuration);
          }
        }
        }
      } catch (error) {
        console.error('Error fetching dashboard data:', error);
      }
    };

    // Fetch dashboard data initially and set up a repeating fetch with a 10-second interval
    fetchData();
    const interval = setInterval(fetchData, 10000);

    // Clean up the interval on component unmount
    return () => clearInterval(interval);
  }, [duration]);

  const minDuration = (brewDuration/60000).toFixed(2);

  return (
    <View style={styles.container}>
    <View style={styles.lastDataContainer}>
      <Text style={{ fontSize: 30,marginBottom: 5,color:'#F1C93B'}}>Last Recorded Data:</Text>

      <View style={styles.temperature}>
        <Icon name="clock-o" size={50} color="#F1C93B"/>
        <Text style={styles.lastDataText}>{minDuration || 'N/A'}</Text>
        <Text style={styles.lastDataText2}>Brewing Duration</Text>
      </View>

      <View style={styles.temperature}>
        <Icon name="thermometer-empty" size={50} color="#F1C93B" />
        <Text style={styles.lastDataText}>{dashboardData.T || 'N/A'}{degreeSymbol}</Text>
          <Text style={styles.lastDataText2}>
          Temperature
         </Text>
      </View>

      <View style={styles.temperature}>
        <Icon name="gears" size={50} color="#F1C93B"/>
        <Text style={styles.lastDataText}>{dashboardData.WP || 'N/A'}</Text>
        <Text style={styles.lastDataText2}>Water Pump</Text>
      </View>

      <View style={styles.temperature}>
      <Icon name="beer" size={50} color="#F1C93B"/>
      <Text style={styles.lastDataText}>{dashboardData.BP || 'N/A'}</Text>
        <Text style={styles.lastDataText2}>Brewing Process</Text>
      </View>

      <View style={styles.temperature}>
      <Icon name="thermometer" size={50} color="#F1C93B"/>
        <Text style={styles.lastDataText}>{dashboardData.BTemp || 'N/A'}{degreeSymbol}</Text>
        <Text style={styles.lastDataText2}>
        Brewing Temperature
        </Text> 
        </View>
    </View>
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
 
  },
  gridContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  chartContainer: {
    height: 450,
    marginTop: 20,
  },
  lastDataContainer: {
    marginTop: 30,
    borderTopWidth: 1,
    paddingTop: 10,
  },
  lastDataText: {
    fontSize: 30,
    marginBottom: 5,
    color:'#FFF'
  },
  lastDataText2: {
    fontSize: 15,
    marginBottom: 5,
    color:'#F1C93B'
  },
  temperature: {
backgroundColor:'#B73E3E',
height:220,
marginBottom:10,
paddingLeft:30,
paddingTop:50,
  },
  // Additional styles for StatBox components and LineChart component can be added here
});

export default Dashboard;
