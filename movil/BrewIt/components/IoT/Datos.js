import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';

const Datos = (props) => {  
    const navigation = useNavigation();

    return (
        <View style={{ marginTop: 50 }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Ionicons name="chevron-back-circle" size={30} />
            </TouchableOpacity>

            <Text>User: {props.userId}</Text>
            <Text>Selected Beer: {props.selectedBeer}</Text>
            <Text>Mode: {props.mode}</Text>
            <Text>
                Ingredients: {props.ingredients.join(', ')}
            </Text>
        </View>
    );
}

export default Datos;
