import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Text, ScrollView, Modal } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useRoute } from '@react-navigation/native';
import Paho from 'paho-mqtt';
import LineChartComponent from './LineChart';
import Dashboard from './dashboard';
import Datos from './Datos';

const MQTT_Form = () => {
    const route = useRoute();
    const userId = route.params.userId;
    const [Setpoint, setSetpoint] = useState('');
    const [BrewingDuration, setBrewingDuration] = useState('');
    const [brewingActive, setBrewingActive] = useState(false);
    const [showGraph, setShowGraph] = useState(false);
    const [seriesData, setSeriesData] = useState([]);
    const [startDateTime, setStartDateTime] = useState(''); // State to store StartDateTime
    const navigation = useNavigation();
    // new states
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectedBeer, setSelectedBeer] = useState(null);
    const [mode, setMode] = useState('');
    const [ingredients, setIngredients] = useState(null);
    const [showData, setShowData] = useState(false);
    const [fermenting, setFermenting] = useState('');
    const [macerating, setMacerating] = useState('');

/*
  // Function to start brewing
  const handleStartBrewing = () => {
    const brewingDurationInMilliseconds = parseInt(BrewingDuration, 10) * 60000;
    const mqttClient = new Paho.Client('broker.hivemq.com', 8000, `Client${userId}M`);

    mqttClient.connect({
      onSuccess: () => {
        // console.log(brewingDurationInMilliseconds);
        console.log('Connected to MQTT broker');
        const setpointTopic = `/Setpoint/${userId}/`;
        const brewingdurationTopic = `/BrewingDuration/${userId}/`;
        mqttClient.publish(setpointTopic, Setpoint, 0, false);
        mqttClient.publish(brewingdurationTopic, brewingDurationInMilliseconds.toString(), 0, false);
        mqttClient.publish(`/StopBrewing/${userId}/`, '1', 0, false);
        mqttClient.disconnect();
      },
      onFailure: (error) => {
        console.log('MQTT connection failed:', error);
      },
    });

    setShowGraph(true);
    setBrewingActive(true);
  };
*/


  const beers = {
      "beer1": {
          name: 'Lager Argentina',
          nickname: '(Pampean Golden)',
          ingredients: ['5kg of Malt', '25g of Hops', 'Safale Yeast'],
          process: `- Maceration: Macerate the malt at 66 ºC for 1 hour to obtain the must. Use 15 liters of water.

- Boiling: Add water to the obtained juice until reaching 27 liters. Boil for 60 minutes adding hops.

- Fermentation: Ferment for about 7 days with high fermentation yeast (18 to 24 ºC).`,
          image: 'https://static.vecteezy.com/system/resources/previews/009/108/008/original/beer-logo-free-vector.jpg',
          maceratingDetails: {
            setpoint: 66,
            duration: 60 * 60000,
            boiling: 60 * 60000,
            brewingDuration: ((60 + 60) + 20) * 60000 // ((1h (maceration) + 60m (boiling)) + 20) * 60000 (1m = 60000ms) = 2h20m
          },
          brewingDetails: {
            setpoint: 21,
            duration: 0,
            boiling: 0,
            brewingDuration: 0,
          },
      },

      "beer2": {
          name: 'Lager Alemana',
          nickname: '(Weissbier - Wheat Beer)',
          ingredients: ['2.5kg Wheat Malt', '20g Hops', 'Safbrew Yeast'],
          process: `- Maceration: Place 12.5 liters of water in the maceration vat. Mash the wheat and pilsner malt together for one hour at 65ºC. A must will be obtained that will be used in the boil.

- Boiling: Combine 27 liters of water and bring the must to a boil. Add hops as above when it comes to a boil.

- Fermentation: Fermentation with high fermentation yeasts is usually at around 22 ºC.`,
          image: 'https://static.vecteezy.com/system/resources/previews/009/108/008/original/beer-logo-free-vector.jpg',
          maceratingDetails: {
            setpoint: 65,
            duration: 60 * 60000,
            boiling: 60 * 60000,
            brewingDuration: ((60 + 60) + 20) * 60000 // ((1h (maceration) + 60m (boiling)) + 20) * 60000 (1m = 60000ms) = 2h20m
          },
          brewingDetails: {
            setpoint: 22,
            duration: 0,
            boiling: 0,
            brewingDuration: 0,
          },
      },

      "beer3": {
          name: 'Ale Inglesa',
          nickname: '(India Pale Ale)',
          ingredients: ['5kg Pale Malt', '100g Hops', '1 tablespoon Irish Moss', 'Special Ale Yeast for IPA'],
          process: `- Maceration: Use 13.9 liters of water to mash the pale and crystal malts. Macerate at a temperature of 65 ºC, for one hour.

- Boiling: Bring 27 liters of water and wort to a boil and add the hops at the indicated times. Total duration of boiling 1 hour and 10 minutes.

- Fermentation: Ferment at the temperature indicated according to yeast. Being ale yeast it will be about 18 ºC. Four days after achieving the correct attenuation (conversion of sugars into alcohol) the temperature can be increased by 1 ºC up to 22 ºC.`,
          image: 'https://static.vecteezy.com/system/resources/previews/009/108/008/original/beer-logo-free-vector.jpg',
          maceratingDetails: {
            setpoint: 65,
            duration: 60 * 60000,
            boiling: 70 * 60000,
            brewingDuration: ((60 + 70) + 20) * 60000 // ((1h (maceration) + 70m (boiling)) + 20) * 60000 (1m = 60000ms) = 2h20m
          },
          brewingDetails: {
            setpoint: 20,
            duration: 0,
            boiling: 0,
            brewingDuration: 0,
          },
      },
  };  

    // Función específica para enviar mensajes MQTT relacionados con Brewing
    const sendMQTTBrewingMessages = (details) => {
        sendMQTTMessages(details, 'Brewing');
    };

    // Función específica para enviar mensajes MQTT relacionados con Maceration
    const sendMQTTMaceratingMessages = (details) => {
        sendMQTTMessages(details, 'Maceration');
    };


    const handleStartBrewing = () => {
    if (!selectedBeer) return;
    const details = selectedBeer.brewingDetails;
    sendMQTTBrewingMessages(details);
    setIsModalVisible(false);
};

const handleStartMacerating = () => {
    if (!selectedBeer) return;
    const details = selectedBeer.maceratingDetails;
    sendMQTTMaceratingMessages(details);
    setIsModalVisible(false);
};


    const sendMQTTMessages = (details, mode) => {
        const mqttClient = new Paho.Client('broker.hivemq.com', 8000, `Client${userId}M`);

        const messages = [
            { 
                payload: JSON.stringify({
                    phone: userId,
                    mode: mode.toUpperCase(),
                    beer: selectedBeer.name,
                    ingredients: selectedBeer.ingredients,
                    macerationTime: mode === 'Maceration' ? details.duration : 0,
                    boilingTime: mode === 'Maceration' ? details.boiling : 0
                }),
                topic: `/brewIt/mode/app/`
            },
            {
                payload: details.setpoint.toString(),
                topic: `/Setpoint/${userId}/`
            },
            {
                payload: details.brewingDuration.toString(),
                topic: `/BrewingDuration/${userId}/`
            }
        ];

        function sendNextMessage() {
            if (messages.length === 0) {
                mqttClient.disconnect();
                console.log("Disconnected from MQTT broker");
                return;
            }

            const messageDetails = messages.shift();
            const message = new Paho.Message(messageDetails.payload);
            message.destinationName = messageDetails.topic;

            mqttClient.send(message);
            console.log(`Published to ${messageDetails.topic} with value:`, messageDetails.payload);

            // Use a timeout to allow for potential round-trip delays
            setTimeout(sendNextMessage, 500);
        }

        mqttClient.connect({
            onSuccess: () => {
                console.log('Connected to MQTT broker');
                sendNextMessage();
            },
            onFailure: (error) => {
                console.log('MQTT connection failed:', error);
            },
        });

        setShowGraph(true);
        setBrewingActive(true);
    };


    
    

    // Function to stop brewing
    const handleStopBrewing = () => {
        const mqttClient = new Paho.Client('broker.hivemq.com', 8000, `Client${userId}`);

        mqttClient.connect({
        onSuccess: () => {
            console.log('Connected to MQTT broker');
            mqttClient.publish(`/StopBrewing/${userId}/`, '0', 0, false);
            mqttClient.disconnect();
        },
        onFailure: (error) => {
            console.log('MQTT connection failed:', error);
        },
    });

    setShowGraph(false);
        setBrewingActive(false);
    };

    const handleProcess = () => {
        setIsModalVisible(false);
        setShowData(true);
    // <Datos userId={userId} selectedBeer={selectedBeer.name} processType={processType} />
    };



  return (
    <View style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
      {brewingActive ? (
        <>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Text style={{fontSize: 50,marginBottom: 5,color:'#B73E3E',alignContent:'center'}}>DASHBOARD</Text>
            <LineChartComponent data={seriesData} userId={userId} />
            <Dashboard userId={userId} />
            <TouchableOpacity onPress={handleStopBrewing} style={{ backgroundColor: 'red', padding: 10 }}>
                <Text style={{ color: '#FFF', textAlign: 'center' }}>Stop Brewing</Text>
            </TouchableOpacity>
        </ScrollView>
        </>
        /*
      ) : showData ? (
        <Datos userId={userId} selectedBeer={selectedBeer.name} mode={mode} ingredients={ingredients} />
        */
      ) : (
        <>
            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Ionicons name="chevron-back-circle" size={30} />
                </TouchableOpacity>

                <View style={{ marginTop: 60 }}>
                    <Ionicons name="beer-outline" size={100} color="#CD0000" style={{ alignSelf: "center" }} />
                    <Text style={styles.welcomeTitle}>
                        Welcome to {"\n"}<Text style={styles.brewitTitle}>BrewIt System!</Text>
                    </Text>

                    <View style={styles.phoneContainer}>
                        <Text style={styles.phoneText}> 
                            Your phone number: {"\n"}<Text style={styles.phone}>{userId}</Text>
                        </Text>
                    </View>
                </View>
            </View>

            <Text style={styles.selectText}>Select a beer:</Text>
            <View style={styles.beerContainer}>
                {Object.keys(beers).map(beerKey => (
                    <TouchableOpacity key={beerKey} style={styles.beerBox} onPress={() => {setSelectedBeer(beers[beerKey]); setIngredients(beers[beerKey].ingredients); setIsModalVisible(true);}}>
                        <Text style={styles.beerName}>{beers[beerKey].name}</Text>
                        <Image source={{ uri: beers[beerKey].image }} style={styles.beerImage} />
                    </TouchableOpacity>
                ))}
            </View>
            {/*
            <TouchableOpacity onPress={handleStartBrewing} style={styles.button}>
                <Text style={styles.textButton}>Start Brewing!</Text>
            </TouchableOpacity>
            */}
        </>
      )}

      <Modal
          animationType="slide"
          transparent={true}
          visible={isModalVisible}
          onRequestClose={() => {
            setIsModalVisible(!isModalVisible);
          }}
      >
          <View style={styles.centeredView}>
              <View style={styles.modalView}>
                  <TouchableOpacity 
                      style={styles.closeButton} 
                      onPress={() => setIsModalVisible(false)}
                  >
                      <Ionicons name="close-circle" size={30} color="black" />
                  </TouchableOpacity>

                    <Text style={styles.modalTitle}>{selectedBeer?.name}</Text>
                    <Text style={styles.modalSubtitle}>{selectedBeer?.nickname}</Text>
                    <ScrollView style={{ marginBottom: 60 }}>
                        <Text style={styles.modalLabel}>Ingredients:</Text>
                        <View>
                            {selectedBeer?.ingredients.map((ingredient, index) => (
                                <Text key={index} style={styles.ingredientItem}>
                                    •  {ingredient}
                                </Text>
                            ))}
                        </View>
                        <Text>{" "}</Text>
                        <Text style={styles.modalLabel}>Process:</Text>
                        <Text>{selectedBeer?.process}</Text>
                    </ScrollView>

                  <View style={styles.buttonContainer}>
                      <TouchableOpacity style={styles.brewingButton} onPress={handleStartBrewing}>
                          <Text style={styles.textButton}>Brewing</Text>
                      </TouchableOpacity>

                      <TouchableOpacity style={styles.macerateButton} onPress={handleStartMacerating}>
                          <Text style={styles.textButton}>Macerate</Text>
                      </TouchableOpacity>
                  </View>
              </View>
          </View>
      </Modal>
    </View>
  );
};



const styles = StyleSheet.create({
  container: {
      marginTop: -80, 
      marginBottom: 60,
  },
  welcomeTitle: {
      textAlign: "center", 
      //fontFamily: "regular",
      fontSize: 32,
      color: "#000",
  },
  brewitTitle: {
      //fontFamily: "bold",
      fontWeight: "bold",
      color: "#CD0000"
  },
  phoneContainer: {
      alignItems: "center", 
      marginTop: 40
  },
  phoneText: {
      //fontFamily: "regular",
      textAlign: "center", 
      fontSize: 16
  },
  phone: {
      //fontFamily: "semibold"
      fontWeight: "bold"
  },
  selectText: {
      marginLeft: 6, 
      fontSize: 16, 
      //fontWeight: "bold", 
      //color: "gray",
      marginTop: -15,
      marginBottom: 15,
      textAlign: "center"
  },
  beerContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
  },
  beerBox: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 5,
      padding: 10,
      borderWidth: 1,
      borderColor: '#cd0000',
      borderRadius: 8,
      backgroundColor: "#fff",
  },
  beerName: {
      fontSize: 17,
      textAlign: "center",
      fontWeight: "bold",
      marginBottom: 10
  },
  beerImage: {
      width: 100,
      height: 100,
      resizeMode: 'contain',
      borderRadius: 10,
  },  
  centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,
      backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalView: {
      width: '90%',
      height: '65%',
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      padding: 35,
      alignItems: 'flex-start',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
  },
  closeButton: {
      position: 'absolute',
      right: 10,
      top: 10,
  },  
  modalTitle: {
      fontSize: 24,
      fontWeight: 'bold',
      color: "#CD0000",
      //marginBottom: 15,
  },
  modalSubtitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 15,
  },
  modalLabel: {
      fontSize: 15,
      fontWeight: "bold",
      fontStyle: "italic" 
  },
  ingredientItem: {
      marginBottom: 5,
  },  
  buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      position: "absolute",
      alignSelf: "center",
      bottom: 10
  },
  brewingButton: {
      backgroundColor: '#cd0000',
      padding: 15,
      margin: 10,
      borderRadius: 10,
  },
  macerateButton: {
      backgroundColor: '#2196F3',
      padding: 15,
      margin: 10,
      borderRadius: 10,
  },  
  button: {
      backgroundColor: "#cd0000", 
      padding: 10
  },
  textButton: {
      //fontFamily: "semibold",
      fontWeight: "bold",
      fontSize: 17,
      color: 'white', 
      textAlign: 'center',
      //marginTop: 3
  }
});

export default MQTT_Form;
