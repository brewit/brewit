
// NOT USED

import { View, Text } from "react-native";
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Search from '../screens/Search';
import Community from '../screens/Community';
import Profile from '../screens/Profile';
import { Ionicons } from "@expo/vector-icons"
import { Colors } from "../components/styles";
import { useNavigation } from '@react-navigation/native';

const Tab = createBottomTabNavigator();

const screenOptions = {
    tabBarShowLabel: false,
    tabBarHideOnKeyboard: true,
    headerShown: false,
    tabBarStyle: {
        position: "absolute",
        bottom: 0,
        right: 0,
        left: 0,
        elevation: 0,
        height: 70
    } 
}

const BottomNavBar = () => {
  const navigation = useNavigation();

    return(
        <Tab.Navigator screenOptions={screenOptions}>
            <Tab.Screen 
                name="Welcome" 
                component={Welcome} 
                options={{
                    tabBarIcon: ({ focused }) => {
                        return(
                            <Ionicons 
                                name={focused ? "home" : "home-outline"} 
                                size={24} 
                                color={focused ? Colors.brand : Colors.tertiary}
                            />
                        )
                    }
                }}
            />

            <Tab.Screen 
                name="Search" 
                component={Search} 
                options={{
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Ionicons 
                                name={focused ? "search-sharp" : "search-sharp-outline"} 
                                size={24} 
                                color={focused ? Colors.brand : Colors.tertiary}
                            />
                        )
                    }
                }}
            />

            <Tab.Screen 
                name="Community" 
                component={Community} 
                options={{
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Ionicons 
                                name={focused ? "newspaper" : "newspaper-outline"}
                                size={24} 
                                color={focused ? Colors.brand : Colors.tertiary}
                            />
                        )
                    }
                }}
            />

            <Tab.Screen 
                name="Profile" 
                component={Profile} 
                options={{
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Ionicons 
                                name={focused ? "person" : "person-outline"} 
                                size={24} 
                                color={focused ? Colors.brand : Colors.tertiary}
                            />
                        )
                    }
                }}
            />

        </Tab.Navigator>
    )
}

export default BottomNavBar;






/*
import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Colors } from './styles';
import { useNavigation } from '@react-navigation/native';

const BottomNavBar = ({ navigation }) => {
  const Navigation = useNavigation();

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 60,
        backgroundColor: 'white',
      }}
    >
      <TouchableOpacity onPress={() => navigation.navigate('Welcome')}>
        <Ionicons name="home" size={30} color={Colors.brand} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Search')}>
        <Ionicons name="search" size={30} color={Colors.brand} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Community')}>
        <Ionicons name="newspaper" size={30} color={Colors.brand} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
        <Ionicons name="person" size={30} color={Colors.brand} />
      </TouchableOpacity>
    </View>
  );
};

export default BottomNavBar;
*/
