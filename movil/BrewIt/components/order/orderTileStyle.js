import { StyleSheet } from 'react-native';
import { COLORS, SIZES, SHADOWS } from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 10,
    flexDirection: "row",
    padding: SIZES.medium,
    paddingTop: -8,
    paddingBottom: -8,
    borderRadius: SIZES.small+10,
    backgroundColor: "#FFF",
    ...SHADOWS.medium,
    shadowColor: COLORS.lightWhite,
  },
  orderTileContainer: {
    flex: 1,
    marginHorizontal: SIZES.medium,
    marginVertical: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  orderTitle: {
    fontSize: 18,
    fontFamily: 'semibold',
    marginBottom: -2,
  },
  orderDescription: {
    fontSize: 16,
  },
  orderReceived: {
    fontSize: 16,
    color: 'blue',
  },
  textContainer: {
    flex: 1,
    marginHorizontal: SIZES.xSmall-8,
    marginBottom: 9,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  orderWithIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: -10,
    marginRight: 20,
    marginTop: 30,
    marginBottom: 30
  },  
  /*
  label: {
    fontSize: 12,
    fontFamily: 'medium',
    color: 'black',
  },
  */
  value: {
    fontSize: 12,
    fontFamily: 'medium',
    color: 'grey', 
  },
  labelTotal: {
    fontSize: 12,
    fontFamily: 'medium',
    color: 'black',
    marginBottom: -5
  },
  labelDate: {
    fontSize: 12,
    fontFamily: 'medium',
    color: 'black',
  },
  labelStatus: {
    fontSize: 12,
    fontFamily: 'medium',
    color: 'black',
    marginTop: -55
  },
  valueStatus: {
    fontSize: 12,
    fontFamily: 'medium',
  },
});

export default styles;