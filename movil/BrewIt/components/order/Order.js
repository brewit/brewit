import React, { useEffect, useState, useContext } from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons } from "@expo/vector-icons";
import styles from './orderStyle';
import { useNavigation } from '@react-navigation/native';
import OrderTile from './OrderTile';
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';

const Order = () => {
  const navigation = useNavigation();
  const { storedCredentials } = useContext(CredentialsContext);
  const [orders, setOrders] = useState([]);

  const fetchOrders = async () => {
    if (!storedCredentials.token) return;
  
    try {
      const response = await axios.get(`${baseURL}/api/orders`, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
        },
      });
      setOrders(response.data); // Actualizar la lista de ordenes
    } catch (e) {
      console.log('Failed to fetch orders', e);
    }
  };

  useEffect(() => {
    fetchOrders();
  }, [storedCredentials]);

  return (
    <SafeAreaView style={styles.orderContainer}>
      <View style={styles.upperRow}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="chevron-back-circle" size={30} />
        </TouchableOpacity>
        <Text style={styles.orderText}> Orders </Text>
      </View>

    <View style={{ flex: 1, marginTop: 50, marginBottom: 20 }}>
        <FlatList
            data={orders}
            renderItem={({ item, index }) => 
            <OrderTile 
                item={item} 
                index={index + 1}
                fetchOrders={fetchOrders} 
                navigation={navigation} 
            />}
            keyExtractor={(item) => item._id}
            style={{ marginHorizontal: 22 }}
        />
    </View>

    </SafeAreaView>
  );
};

export default Order;
