import { StyleSheet } from 'react-native';
import { COLORS, SIZES } from '../../constants';

export default StyleSheet.create({
  orderContainer: {
      flex: 1,
      backgroundColor: COLORS.offwhite,
  },
  upperRow: {
      marginHorizontal: 20,
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      top: SIZES.xxLarge,
      width: SIZES.width -44,
      marginTop: -10,
      zIndex: 999
  },
  orderText: {
      fontFamily: "bold",
      fontSize: SIZES.xLarge+5,
      color: COLORS.primary,
      marginTop: 7
  },
});
