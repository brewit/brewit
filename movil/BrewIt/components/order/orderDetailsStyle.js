import { StyleSheet } from 'react-native';
import { COLORS, SIZES, SHADOWS } from '../../constants';

export default StyleSheet.create({
  orderDetailsContainer: {
      flex: 1,
      backgroundColor: "#fff",
  },
  upperRow: {
      marginHorizontal: 20,
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      top: SIZES.xxLarge,
      width: SIZES.width -44,
      marginTop: 10,
      zIndex: 999
  },
  orderDetailsText: {
      fontFamily: "bold",
      fontSize: SIZES.xLarge+5,
      color: COLORS.primary,
      marginTop: 7
  },
  orderDetails: {
      marginHorizontal: 36,
  },
  orderProducts: {
      textAlign: "center",
      fontFamily: "regular",
      fontSize: SIZES.xLarge,
      marginTop: SIZES.xLarge
  },
  orderTitle: {
      fontSize: 24,
      fontWeight: 'bold',
  },
    orderStatus: {
      fontSize: 18,
  },
  orderPrice: {
      fontSize: 18,
      fontWeight: 'bold',
  },
  orderReceived: {
      fontSize: 18,
      color: 'blue',
  },
  orderLabel: {
      fontSize: 18,
      fontWeight: 'bold',
  },
  
  orderValue: {
      fontSize: 18,
  },
  orderTotalValue: {
      fontSize: 18,
      fontWeight: "bold"
  }, 
  container: {
      flex: 1,
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: 10,
      flexDirection: "row",
      padding: SIZES.medium,
      paddingTop: -8,
      paddingBottom: -8,
      borderRadius: SIZES.small+10,
      backgroundColor: "#FFF",
      ...SHADOWS.medium,
      shadowColor: COLORS.lightWhite,
  },
  image: {
      width: 70,
      backgroundColor: COLORS.secondary,
      borderRadius: SIZES.medium,
      justifyContent: "center",
      alignItems: "center"
  },
  productImg: {
      width: "100%",
      height: 65,
      borderRadius: SIZES.small,
      resizeMode: "cover"
  },
  textContainer: {
      flex: 1,
      marginHorizontal: SIZES.medium,
      flexDirection: 'row',
      alignItems: 'flex-end',
  },
  productName: {
      fontSize: SIZES.medium,
      fontFamily: "bold",
      color: COLORS.black,
      marginBottom: -7,
      marginTop: 5
  },
  origin: {
      fontSize: SIZES.small+2,
      fontFamily: "regular",
      color: COLORS.gray,
      marginTop: 3,
  },
  price: {
      fontSize: SIZES.small+2,
      fontFamily: "regular",
      color: COLORS.gray,
  },
  quantity: {
      textAlign: "center",
      fontSize: SIZES.small+2,
      fontFamily: "regular",
      color: COLORS.gray,
      marginTop: -65
  },
  buttonContainer: {
    padding: 10,
    backgroundColor: COLORS.primary,
},
receivedButton: {
    borderRadius: 20,
    alignItems: 'center',
},
  orderReceived: {
      color: 'white',
      fontFamily: "semibold",
      fontSize: 16,
  },
});
