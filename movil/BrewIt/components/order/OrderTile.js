import React, { useContext } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { Ionicons, Fontisto } from "@expo/vector-icons";
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';
import { COLORS } from '../../constants';
import styles from './orderTileStyle';

const OrderTile = ({ item, fetchOrders, navigation, index }) => {
    
  const { storedCredentials } = useContext(CredentialsContext);

  return (
    <View style={styles.container}>
        
        <TouchableOpacity style={styles.orderTileContainer} onPress={() => navigation.navigate('OrderDetails', { item, fetchOrders })}>
    
        <View style={styles.orderWithIcon}>
            <Fontisto name="shopping-bag" size={30} color={COLORS.primary}/>
        </View>

        <View style={styles.textContainer}>
            <View style={{flex: 1}}>
                <Text style={styles.orderTitle}>Order {index}</Text>
                <Text style={styles.labelTotal}>Total: <Text style={styles.value}>${item.total}</Text></Text>
                <Text style={styles.labelDate}>
                    Date: <Text style={styles.value}>{new Date(item.date).toLocaleString('en-US', { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true })}</Text>
                </Text>
            </View>

            <View>
                <Text style={styles.labelStatus}> 
                    Status: <Text style={[styles.valueStatus, { color: item.status === 'received' ? 'grey' : '#CD0000' }]}>{"\n"}{item.status}</Text>
                </Text>
            </View>
        </View>
      </TouchableOpacity>
    </View>
  );
  
};

export default OrderTile;
