import React, { useContext, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, FlatList, Image, SafeAreaView } from 'react-native';
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import styles from './orderDetailsStyle';

const OrderDetails = ({ route }) => {
  const { item: initialItem, fetchOrders } = route.params;
  const [item, setItem] = useState(initialItem);
  const { storedCredentials } = useContext(CredentialsContext);
  const navigation = useNavigation();

  const markOrderReceived = async () => {
    try {
      await axios.put(`${baseURL}/api/orders/${item._id}`, {
        status: 'received',
      }, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
        },
      });
      fetchOrders();
      Toast.show({
        type: 'success',
        position: 'bottom',
        text1: 'Order received successfully!',
        visibilityTime: 3000,
        autoHide: true,
        topOffset: 30,
        bottomOffset: 40,
        text1Style: {
          fontSize: 20,
        },
      });
      setItem({ ...item, status: 'received' });
    } catch (e) {
      console.log('Failed to mark order as received', e);
    }
  };

  useEffect(() => {
    if(initialItem.status !== item.status) {
      setItem(initialItem);
    }
  }, [initialItem]);

  const getImageUri = (path) => {
    if(path.startsWith('/uploads/')) {
        return `${baseURL}${path}`;
    }
    return path;
  }


  return (
    <SafeAreaView style={styles.orderDetailsContainer}>
        <View style={styles.upperRow}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Ionicons name="chevron-back-circle" size={30} />
            </TouchableOpacity>
            <Text style={styles.orderDetailsText}> Order Details </Text>
        </View>

        <View style={{ flex: 1, marginTop: 60 }}>
        <View style={styles.orderDetails}>
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={styles.orderLabel}>Date:</Text>
                <Text style={styles.orderValue}>
                    {new Date(item.date).toLocaleString('en-US', { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true })}
                </Text>
            </View>

            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={styles.orderLabel}>Card:</Text>
                <Text style={styles.orderValue}>**** **** **** {item.cardLastFour}</Text>
            </View>

            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={styles.orderLabel}>Status:</Text>
                <Text style={[styles.orderValue, { color: item.status === 'received' ? 'black' : '#CD0000' }]}>{item.status}</Text>
            </View>

            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                <Text style={styles.orderLabel}>Total:</Text>
                <Text style={styles.orderTotalValue}>${item.total}</Text>
            </View>
        </View>

            <Text style={styles.orderProducts}> Products </Text>
            <FlatList 
                data={item.products}
                renderItem={({ item }) => (
                <>
                    <View style={styles.container}>
                        <View style={styles.image}>
                            <Image 
                                source={{uri: getImageUri(item.photo) }}
                                style={styles.productImg}
                            />
                        </View>

                        <View style={styles.textContainer}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.productName}>{item.name}</Text>
                                <Text style={styles.origin}>Origin: {item.origin}</Text>
                                <Text style={styles.price}>Price: ${item.price}</Text>
                            </View>

                            <View>
                                <Text style={styles.quantity}>Quantity: {"\n"}{item.quantity}</Text>
                            </View>
                        </View>
                    </View>
                </>
                )}
                keyExtractor={(item) => item.productId}
                // style={{ marginHorizontal: 12 }}
            />
        </View>
        {item.status !== 'received' && (
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.receivedButton} onPress={markOrderReceived}>
                    <Text style={styles.orderReceived}>Mark as Received</Text>
                </TouchableOpacity>
            </View>
        )}
    </SafeAreaView>
  );
};

export default OrderDetails;
