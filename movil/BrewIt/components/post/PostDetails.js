import React, { useState, useEffect, useContext } from 'react';
import { View, Text, TextInput, TouchableOpacity, Image, SafeAreaView, ScrollView } from 'react-native';
import { COLORS, SIZES } from '../../constants';
import { Ionicons } from "@expo/vector-icons";
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';
import styles from './postDetailsStyle';

const PostDetails = ({ route }) => {
    const { postId } = route.params;
    const [post, setPost] = useState(null);
    const [comments, setComments] = useState([]);
    const [newComment, setNewComment] = useState("");
    const { storedCredentials } = useContext(CredentialsContext);
    const navigation = useNavigation();

    useEffect(() => {
        const fetchPost = async () => {
            if (!storedCredentials.token) return;

            try {
                const response = await axios.get(`${baseURL}/api/posts/${postId}`, {
                    headers: {
                        Authorization: `Bearer ${storedCredentials.token}`,
                    },
                });
                setPost(response.data);
            } catch (e) {
                console.log('Failed to fetch post', e);
            }
        };

        fetchPost();
    }, [postId, storedCredentials]);


    useEffect(() => {
        const fetchComments = async () => {
            if (!storedCredentials.token) return;

            try {
                const response = await axios.get(`${baseURL}/api/posts/${postId}/comments`, {
                    headers: {
                        Authorization: `Bearer ${storedCredentials.token}`,
                    },
                });
                setComments(response.data);
            } catch (e) {
                console.log('Failed to fetch comments', e);
            }
        };

        fetchComments();
    }, [postId, storedCredentials]);


    const submitComment = async () => {
        try {
            const response = await axios.post(`${baseURL}/api/posts/${postId}/comments`, {
                content: newComment,
            }, {
                headers: {
                    Authorization: `Bearer ${storedCredentials.token}`,
                },
            });
            setComments([...comments, response.data]);
            setNewComment("");
        } catch (e) {
            console.log('Failed to submit comment', e);
        }
    };
    
    if (!post) {
        return (
            <View style={styles.loadingContainer}>
                <Text style={styles.loadingText}>Loading...</Text>
            </View>
        );
    }


    return (
        <SafeAreaView style={styles.postDetailsContainer}>
            <ScrollView>
                <View style={styles.upperRow}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Ionicons name="chevron-back-circle" size={30} />
                    </TouchableOpacity>
                    <Text style={styles.postDetailsText}> Post Details </Text>
                </View>

                <View style={styles.container}>
                    <View style={styles.postContainer}>
                        <Image source={require("../../assets/img/def.jpg")} style={styles.avatar} />
                        <View>
                            <View style={styles.userInfo}>
                                <Text style={styles.username}>{post.user.name}</Text>
                                {post.user.isAdmin && (
                                    <Ionicons 
                                        name="checkmark-circle" 
                                        size={20} 
                                        color={COLORS.primary} 
                                        style={{ marginTop: -4, marginLeft: -4 }}
                                    />
                                )}
                                <Text style={styles.email}>
                                    @{post.user.email && post.user.email.split('@')[0]}
                                </Text>
                            </View>
                            <Text style={styles.text}>{post.content}</Text>
                        </View>
                    </View>

                    <View style={styles.divider} />

                    {comments.length === 0 ? (
                        <Text style={styles.noCommentsText}>No comments yet</Text>
                    ) : (
                        comments.map(comment => (
                            <View key={comment._id} style={styles.commentContainer}>
                                <Image source={require("../../assets/img/def.jpg")} style={styles.avatar} />
                                <View>
                                    <View style={styles.userInfo}>
                                        <Text style={styles.username}>{comment.user.name}</Text>
                                        {comment.user.isAdmin && (
                                            <Ionicons 
                                                name="checkmark-circle" 
                                                size={20} 
                                                color={COLORS.primary} 
                                                style={{ marginTop: -4, marginLeft: -4 }}
                                            />
                                        )}
                                        <Text style={styles.email}>
                                            @{comment.user.email && comment.user.email.split('@')[0]}
                                        </Text>
                                    </View>
                                    <Text style={styles.text}>{comment.content}</Text>
                                </View>
                            </View>
                        ))
                    )}
                </View>
            </ScrollView>

            <View style={styles.inputContainer}>
                <TextInput
                    placeholder="Write a comment..."
                    value={newComment}
                    onChangeText={setNewComment}
                    style={styles.input}
                />
                <TouchableOpacity style={styles.button} onPress={submitComment}>
                    <Text style={styles.buttonText}>Submit</Text>
                </TouchableOpacity>
            </View>
            
        </SafeAreaView>
    );
};

export default PostDetails;