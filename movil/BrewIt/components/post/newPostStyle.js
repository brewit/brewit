import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    post: {
        marginTop: 200,
        justifyContent: "center",
        alignItems: "center"
    },
    postTitle:{
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 20,
        backgroundColor: COLORS.lighterGray,
        marginTop: -150
    },
    input: {
        height: 150,
        borderColor: COLORS.primary,
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 20,
        padding: 10,
        textAlignVertical: 'top',
        backgroundColor: COLORS.white,
        fontSize: SIZES.medium,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 5,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    postButton: {
        flex: 1,
        backgroundColor: COLORS.primary,
        padding: 10,
        alignItems: 'center',
        borderRadius: 10,
        marginRight: 5,
        marginHorizontal: 80
    },
    postButtonText: {
        fontFamily: "semibold",
        color: COLORS.white,
        fontSize: SIZES.large
    },
    cancelButton: {
        flex: 1,
        backgroundColor: COLORS.gray,
        alignItems: 'center',
        borderRadius: 10,
        marginLeft: 5,
    },
    cancelButtonText: {
        fontFamily: "semibold",
        color: COLORS.white,
        fontSize: SIZES.large,
        marginTop: 10 
    }
});

export default styles;
