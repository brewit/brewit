import React, { useState, useContext } from 'react';
import { Text, View, TextInput, Alert, TouchableOpacity, SafeAreaView } from 'react-native';
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';
import { useNavigation } from '@react-navigation/native';
import styles from './newPostStyle';


const NewPost = () => {
    const { storedCredentials } = useContext(CredentialsContext);
    const [content, setContent] = useState('');
    const navigation = useNavigation();

    const handlePost = async () => {
        if (!storedCredentials.token) return;
        if (content === '') return;

        try {
            await axios.post(`${baseURL}/api/posts`, { content }, {
                headers: {
                    Authorization: `Bearer ${storedCredentials.token}`,
                },
            });
        
            setContent(''); 
            Alert.alert(
                "Success",
                "Post shared successfully!",
                [
                    { text: "OK", onPress: () => navigation.navigate('Community') }
                ],
                { cancelable: false } // Esto significa que la alerta no se descartará al tocar fuera de su área.
            );
        } catch (e) {
            console.log('Failed to create post', e);
        }
        
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.post}>
                <Text style={styles.postTitle}> Create a Post </Text>
            </View>

            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    value={content}
                    onChangeText={setContent}
                    multiline
                    placeholder="What's on your mind?"
                />

                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.cancelButton} onPress={() => navigation.navigate('Community')}>
                        <Text style={styles.cancelButtonText}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.postButton} onPress={handlePost}>
                        <Text style={styles.postButtonText}>Post</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default NewPost;
