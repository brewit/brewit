import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    postDetailsContainer: {
        flex: 1,
        backgroundColor: COLORS.lightGray,
    },
    upperRow: {
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        top: SIZES.xxLarge,
        width: SIZES.width -44,
        marginTop: 10,
        zIndex: 999
    },
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.white
    },
    loadingText: {
        fontFamily: 'bold',
        fontSize: SIZES.large,
        color: COLORS.primary,
    },    
    postDetailsText: {
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary,
        marginTop: 7
    },
    container: {
        marginTop: 50
    },
    postContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#fff",
        marginBottom: 10,
        borderRadius: 10,
        elevation: 2,
    },
    commentContainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: "#f1f1f1",
        marginBottom: 10,
        borderRadius: 10,
        elevation: 1,
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10,
    },
    userInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
    },
    username: {
        fontFamily: 'bold',
        marginRight: 5,
    },
    email: {
        color: COLORS.gray,
        fontSize: SIZES.small+2,
        marginTop: -6,
        marginLeft: 1
    },
    text: {
        fontFamily: "regular",
        fontSize: SIZES.medium-1,
    },
    divider: {
        height: 1,
        backgroundColor: COLORS.gray,
        marginVertical: 10,
    },
    inputContainer: {
        padding: 10,
        backgroundColor: '#fff',
    },
    input: {
        height: 40,
        borderColor: COLORS.primary,
        borderWidth: 1,
        borderRadius: 20,
        padding: 10,
        marginBottom: 10,
    },
    button: {
        backgroundColor: COLORS.primary,
        padding: 10,
        alignItems: 'center',
        borderRadius: 15,
        marginHorizontal: 135
    },
    buttonText: {
        color: "#fff",
        fontFamily: "semibold",
        fontSize: SIZES.medium,
        marginBottom: -5 
    },
    noCommentsText: {
        textAlign: 'center',
        marginTop: 20,
        fontSize: SIZES.medium,
        color: COLORS.gray
    }    
});

export default styles;
