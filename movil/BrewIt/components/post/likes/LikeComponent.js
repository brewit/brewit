import React, { useState, useEffect, useContext } from 'react';
import { View, Text, TouchableOpacity, Modal, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';
import baseURL from '../../../env';
import { CredentialsContext } from '../../../components/CredentialsContext';
import { COLORS, SIZES } from '../../../constants';
import styles from './likeComponentStyle';

function LikeComponent({ post, updatePost }) {
    const [modalVisible, setModalVisible] = useState(false);
    const { storedCredentials } = useContext(CredentialsContext);
    const [isLiked, setIsLiked] = useState(false);
    const [likeUsers, setLikeUsers] = useState([]);

    const handleLike = async () => {
        try {
            const response = await axios.put(`${baseURL}/api/posts/${post._id}/toggle-like`, null, {
                headers: {
                    Authorization: `Bearer ${storedCredentials.token}`,
                },
            });
            
            if(response.data.success) {
                console.log('API response:', response.data.data);
                updatePost(response.data.data);
                setIsLiked(response.data.data.likes.includes(storedCredentials._id.toString()));
            } else {
                console.log('Failed to like post');
            }
        } catch (e) {
            console.log('Failed to like post', e);
        }
    };     

    useEffect(() => {
        setIsLiked(post.likes.includes(storedCredentials._id.toString()));
    }, [post.likes, storedCredentials._id]);


    useEffect(() => {
        const fetchLikeUsers = async () => {
            const response = await axios.post(`${baseURL}/api/posts/users/bulk-get`, {
                userIds: post.likes,
            }, {
                headers: {
                    Authorization: `Bearer ${storedCredentials.token}`,
                },
            });
    
            if(response.data.success) {
                setLikeUsers(response.data.data);
            } else {
                console.log('Failed to fetch like users');
            }
        };
        
        fetchLikeUsers();
    }, [post.likes]);
    

    return (
        <View style={styles.iconContainer}>
            <TouchableOpacity 
                style={[styles.iconButton, { flexDirection: 'row', alignItems: 'center' }]} 
                onPress={handleLike}
                onLongPress={() => setModalVisible(true)}
            >
                <Ionicons 
                    name={isLiked ? "heart" : "heart-outline"} 
                    size={26} 
                    color={isLiked ? COLORS.primary : COLORS.gray2} 
                />
                <Text style={{ marginLeft: 5 }}>
                    {post.likes.length > 0 ? post.likes.length : ""}
                </Text>
            </TouchableOpacity>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Ionicons name="heart" color={COLORS.primary} size={28} />
                        <Text style={styles.modalText}>Users who liked this:</Text>
                        <FlatList 
                            data={likeUsers}
                            keyExtractor={(item) => item._id}
                            renderItem={({ item }) => <Text style={styles.modalUsers}>{item.name}</Text>}
                        />
                        <TouchableOpacity onPress={() => setModalVisible(false)} style={styles.modalClose}>
                            <Text style={styles.closeBtn}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
}

export default LikeComponent;
