import { StyleSheet, Dimensions } from "react-native";
import { COLORS, SIZES } from "../../../constants";

const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    iconContainer: {
        flexDirection: 'row',
        //marginTop: 1,
    },
    iconButton: {
        marginRight: 85,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -40,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: screenHeight * 0.5,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        marginVertical: 250,
        paddingHorizontal: 50
    },
    modalText: {
        fontFamily: "semibold",
        fontSize: SIZES.medium,
        marginBottom: 15,
        textAlign: 'center',
    },
    modalUsers: {
        fontFamily: "regular",
        fontSize: SIZES.small+2,
    },
    modalClose: {
        backgroundColor: COLORS.primary,
        borderRadius: 12,
        padding: 12
    },
    closeBtn: {
        fontFamily: "semibold",
        fontSize: SIZES.medium,
        color: "#fff"
    }
});

export default styles;