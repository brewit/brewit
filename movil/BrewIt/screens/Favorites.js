import { Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons } from "@expo/vector-icons";
import styles from './favoritesStyle';
import { useNavigation } from '@react-navigation/native';

const Cart = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView>
      <View style={styles.cartContainer}></View>
        <View style={styles.upperRow}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Ionicons name="chevron-back-circle" size={30} />
            </TouchableOpacity>
            <Text style={styles.cartText}> Favorites </Text>
        </View>
      <View/>
    </SafeAreaView>
  )
}

export default Cart;