import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import React, { useContext } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons, Fontisto } from "@expo/vector-icons";
import styles from './homeStyle';
import { Welcome } from '../components';
import CarouselComponent from '../components/home/Carousel';
import Headings from '../components/home/Headings';
import ProductRow from '../components/products/ProductRow';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { CredentialsContext } from '../components/CredentialsContext';
import { useCart } from '../components/cart/CartContext';
import useFetch from '../hook/useFetch';

const Home = () => {
  const navigation = useNavigation();
  const { storedCredentials, setStoredCredentials } = useContext(CredentialsContext);
  const { name, email } = storedCredentials;
  const { cartItems } = useCart();
  const { data, isLoading, error, refetch } = useFetch();

  useFocusEffect(
    React.useCallback(() => {
      refetch();  // Refresca los productos cada vez que Home gane el foco
    }, [])
  );

  return (
    <SafeAreaView>
      <View style={styles.appBarWrapper}>

        <View style={styles.appBar}>

          <View style={styles.userWrapper}> 
            <Ionicons name="beer-outline" size={24} style={{marginTop: -5}} />
            <Text style={styles.user}> Hi, {name || "user"} </Text>
          </View>
          
          <View style={{ alignItems: "flex-end", marginRight: 10, marginTop: 30 }}>
              {cartItems.length > 0 && (
                <View style={styles.cartCount}>
                  <Text style={styles.cartNumber}>{cartItems.length}</Text>
                </View>
              )}
              <TouchableOpacity onPress={() => navigation.navigate("Cart")}>
                  <Ionicons name="cart" size={30} />
                  {/* <Fontisto name="shopping-bag" size={24} /> */}
              </TouchableOpacity>
          </View>


        </View>

      </View>

      <ScrollView>
        <Welcome/>
        <CarouselComponent/>
        <Headings/>
        <ProductRow data={data} isLoading={isLoading} error={error} />
      </ScrollView>

      <Text style={styles.brand}> ® BrewIt 2023. All rights reserved. </Text>

    </SafeAreaView>
  )
}

export default Home;