import { Text, View, Image, TouchableOpacity, StatusBar, SafeAreaView } from 'react-native';
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { COLORS } from '../constants';
import { Ionicons } from "@expo/vector-icons";
import styles from './welcomePageStyle';

const WelcomePage = () => {
    const navigation = useNavigation();

    return (
      <>
        {/* <StatusBar hidden={true} /> */}
        <LinearGradient
          style={{ flex: 1, width: '100%', height: '100%' }}
          colors={[COLORS.primary, COLORS.primary2]}
        >
            <View style={{ flex: 1 }}>
                <Image
                  source={require("../assets/img/img1.jpeg")}
                  style={styles.img1}
                />

                <Image
                  source={require("../assets/img/beer2.webp")}
                  style={styles.img2}
                />

                <Image
                  source={require("../assets/img/beer1.webp")}
                  style={styles.img3}
                />

                <Image
                  source={require("../assets/img/beer4.jpg")}
                  style={styles.img4}
                />

                <Image
                  source={require("../assets/img/loguito.jpg")}
                  style={styles.img5}
                />

                <View style={styles.container}>
                    <Text style={styles.title}>Let's Get</Text>
                    <Text style={styles.title2}>Started</Text>

                    <View style={styles.textContainer}>
                        <Text style={styles.subtitle}>
                            Connect with others with <Text style={styles.brand}>Brew It</Text>
                        </Text>
                        <Text style={styles.subtitle2}>Brew, learn and share in the same place! :)</Text>
                    </View>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Login")}>
                            <Text style={styles.buttonText}>Join Now</Text>
                            <Ionicons name="arrow-forward" size={34} color={COLORS.primary} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </LinearGradient>
      </>
    )
}

export default WelcomePage;