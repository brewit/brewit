import React, { useContext } from "react";
import { Text, View, TouchableOpacity, Alert } from "react-native";
import { StatusBar } from 'expo-status-bar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CredentialsContext } from './../components/CredentialsContext';
import { Ionicons, Fontisto, MaterialCommunityIcons } from "@expo/vector-icons";
import { COLORS, SIZES } from "../constants";
import { useNavigation } from '@react-navigation/native';
// import BottomNavBar from "../components/BottomNavBar";
import styles from "./profileStyle";
import { 
    InnerContainer, 
    PageTitle, 
    SubTitle, 
    StyledFormArea, 
    StyledButton,
    ButtonText,
    Line,
    WelcomeContainer,
    WelcomeImage,
    Avatar,
    AvatarPhoto
} from './../components/styles';
import * as SplashScreen from 'expo-splash-screen';
import { useCart } from '../components/cart/CartContext';

const Profile = () => {
    const navigation = useNavigation();
    const { storedCredentials, setStoredCredentials } = useContext(CredentialsContext);
    const { _id, name, email, photoUrl, isAdmin } = storedCredentials;
    const AvatarImg = photoUrl ? { uri: photoUrl } : require('./../assets/img/def.jpg');
    const {storeToken} = useCart();

    const clearLogin = () => {
        AsyncStorage.removeItem('brewItCredentials')
            .then(() => {
                setStoredCredentials("");
                storeToken(null);
            })
            .catch(err => console.log(err))
    }
    // Hide the splash screen once the welcome screen has loaded
    SplashScreen.hideAsync();


    return (
        <>
            <StatusBar style="auto" />
            <View style={styles.container}>
                {/* <WelcomeImage resizeMode="cover" source={require('./../assets/img/img2.jpeg')} /> */}
                {/* <WelcomeContainer>
                
                </WelcomeContainer> */}
                <View style={styles.profile}>
                    <Text style={styles.profileTitle}> Profile </Text>
                </View>
                <Avatar>
                    <AvatarPhoto resizeMode="cover" source={AvatarImg} />
                </Avatar>
                    {/* <PageTitle welcome={true}> Welcome dawg! </PageTitle> */}

                <View style={styles.info}>
                    <View style={{ flexDirection: "row" }}>
                        <Text welcome={true} style={styles.name}> {name || "Name"} </Text>
                        {isAdmin && (
                            <Ionicons 
                                name="checkmark-circle" 
                                size={24} 
                                color={COLORS.primary} 
                                style={{ marginTop: 1, marginLeft: -2 }}
                            />
                        )}
                    </View>
                    <Text welcome={true} style={styles.email}> {email || "email@gmail.com"} </Text>
                </View>

                <View style={styles.sections}>
                    <TouchableOpacity style={styles.favorites} onPress={() => /*navigation.navigate("Favorites")*/ Alert.alert("Message", "This tool will be available soon...")}>
                        <Ionicons name="heart-outline" size={30} color={COLORS.primary} />
                        <Text style={styles.favoritesText}> Favorites </Text>
                    </TouchableOpacity>

                    <View style={styles.line2}/>

                    <TouchableOpacity style={styles.orders} onPress={() => navigation.navigate("Order")}>
                        <MaterialCommunityIcons name="truck-delivery-outline" size={30} color={COLORS.primary} style={{marginLeft: 3}}/>
                        <Text style={styles.ordersText}> Orders </Text>
                    </TouchableOpacity>

                    <View style={styles.line2}/>

                    <TouchableOpacity style={styles.cart} onPress={() => navigation.navigate("Cart")}>
                        <Ionicons name="cart-outline" size={30} color={COLORS.primary}/>
                        <Text style={styles.cartText}> Cart </Text>
                    </TouchableOpacity>

                    <View style={styles.line2}/>

                    <TouchableOpacity style={styles.cache} onPress={() => Alert.alert("Message", "Cache cleared!")}>
                        <Ionicons name="sync" size={30} color={COLORS.primary} style={{ marginLeft: 1 }}/>
                        <Text style={styles.cacheText}> Clear Cache </Text>
                    </TouchableOpacity>

                    <View style={styles.line2}/>

                    <TouchableOpacity style={styles.startBrewing} onPress={() => navigation.navigate("MQTT_Form", { userId: _id })}>
                        <Ionicons name="beer-outline" size={30} color={COLORS.primary} style={{ marginLeft: 3 }}/>
                        <Text style={styles.startBrewingText}> Start Brewing </Text>
                    </TouchableOpacity>

                </View>

                <StyledFormArea>
                    {/* <View style={styles.line}/> */}

                    <TouchableOpacity onPress={clearLogin} style={styles.logout}>
                        <ButtonText> Logout </ButtonText>
                    </TouchableOpacity>

                    <Text style={styles.brand}> ® BrewIt 2023. All rights reserved. </Text>
                </StyledFormArea>

            </View>
           
        </>
    );
};

export default Profile;