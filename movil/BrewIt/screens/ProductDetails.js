import { TouchableOpacity, Text, View, Image, Alert } from 'react-native';
import React, { useState, useContext } from 'react';
import { useRoute } from '@react-navigation/native';
import { Ionicons, Fontisto, SimpleLineIcons, MaterialCommunityIcons } from "@expo/vector-icons" 
import styles from './productDetailsStyle';
import { COLORS, SIZES } from '../constants';
import { useCart } from '../components/cart/CartContext';
import { CredentialsContext } from '../components/CredentialsContext';
import baseURL from '../env';
import axios from 'axios';
import Toast from 'react-native-toast-message';


const ProductDetails = ({navigation}) => {
  const [isLiked, setIsLiked] = useState(false);
  const route = useRoute();
  const {item} = route.params;
  const [count, setCount] = useState(1);
  const { cartItems, setCartItems } = useCart();
  const { storedCredentials } = useContext(CredentialsContext);

  const increment = () => {
    setCount(count + 1)
  }

  const decrement = () => {
    if (count > 1){
      setCount(count - 1)
    }
  }

  const handleLike = () => {
    setIsLiked(!isLiked);
  };

  const handleBuyNow = async () => {
    const uniqueId = Date.now();
    const itemWithQuantity = { ...item, quantity: count, id: uniqueId };
    setCartItems([...cartItems, itemWithQuantity]);
    try {
        const response = await axios.post(`${baseURL}/api/cart`, {
          userId: storedCredentials._id, // Aquí pasas el número de teléfono del usuario
          productId: uniqueId,
          name: item.name,
          origin: item.origin,
          price: item.price,
          photo: item.photo,
          quantity: count
      }, {
          headers: {
              Authorization: `Bearer ${storedCredentials.token}` // Aquí pasas el token del usuario
          }
    });
        // Alert.alert("Alert", "Product added to Cart successfully!");
        Toast.show({
          type: 'success',
          position: 'bottom',
          text1: 'Product added to Cart successfully!',
          visibilityTime: 3000,
          autoHide: true,
          topOffset: 30,
          bottomOffset: 40,
          text1Style: {
            fontSize: 20,
          },
        });
        console.log(response.data);
    } 
    catch (err) {
        Alert.alert("Error", err.message || "An error occurred");
        console.log(err);
    }
  };

  const getImageUri = (path) => {
    if(path.startsWith('/uploads/')) {
        return `${baseURL}${path}`;
    }
    return path;
  }



    return (
        <View style={styles.container}>
          <View style={styles.upperRow}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Ionicons name="chevron-back-circle" size={30} />
            </TouchableOpacity>

            <TouchableOpacity onPress={handleLike}>
            {isLiked ? (
              <Ionicons name="heart" size={30} color={COLORS.primary} />
            ) : (
              <Ionicons name="heart-outline" size={30} color={COLORS.primary} />
            )}
            </TouchableOpacity>
          </View>

          <Image 
            source={{ uri: getImageUri(item.photo) }}
            style={styles.image}
          />

          <View style={styles.details}>

            <View style={styles.titleRow}>
              <Text style={styles.title}> {item.name}</Text>
              <View style={styles.priceWrapper}>
                <Text style={styles.price}>${item.price}</Text>
              </View>
            </View>

            <View style={styles.ratingRow}>
              <View style={styles.rating}>
                {[1, 2, 3, 4, 5].map((index) => (
                  <Ionicons
                    key={index}
                    name="star"
                    size={24}
                    color="gold"
                  />
                ))}
                <Text style={styles.ratingText}>(4.9)</Text>
              </View>


              <View style={styles.rating}>
                <TouchableOpacity onPress={() => increment()}>
                  <SimpleLineIcons name="plus" size={20}/>
                </TouchableOpacity>

                <Text style={styles.ratingText}>{count}</Text>

                <TouchableOpacity onPress={() => decrement()}>
                  <SimpleLineIcons name="minus" size={20}/>
                </TouchableOpacity>
              </View>

            </View>

            <View style={styles.descriptionWrapper}>
                <Text style={styles.description}> Description </Text>
                <Text style={styles.descText}>{item.description} </Text>
            </View>

            <View style={{ marginBottom: SIZES.small }}>
                <View style={styles.location}>
                    <View style={{ flexDirection: "row" }}>
                        <Ionicons name="location-outline" size={20} />
                        <Text>  {item.origin} </Text>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                        <MaterialCommunityIcons name="truck-delivery-outline" size={20} />
                        <Text>  Free Delivery! </Text>
                    </View>
                </View>
            </View>

            <View style={styles.cartRow}>
                  <TouchableOpacity onPress={handleBuyNow} style={styles.cartBtn}>
                      <Text style={styles.cartTitle}> ADD TO CART </Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => navigation.navigate("Cart")} style={styles.addCart}>
                      <Fontisto name="shopping-bag" size={22} color={COLORS.lightWhite} />
                  </TouchableOpacity>
            </View>

          </View>

        </View>
    )
}

export default ProductDetails;