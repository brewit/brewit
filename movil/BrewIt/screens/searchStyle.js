import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../constants";

const styles = StyleSheet.create({
    search: {
        marginTop: SIZES.medium,
        justifyContent: "center",
        alignItems: "center"
    },
    searchTitle:{
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary
    },
    searchContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: SIZES.small,
        backgroundColor: COLORS.secondary,
        borderRadius: SIZES.medium,
        marginVertical: SIZES.medium,
        height: 50
    },
    searchIcon: {
        marginHorizontal: 10,
        color: COLORS.gray,
    },
    searchWrapper: {
        flex: 1,
        backgroundColor: COLORS.secondary,
        marginRight: SIZES.small,
        borderRadius: SIZES.small

    },
    searchInput: {
        fontFamily: "regular",
        width: "100%",
        height: "100%",
        paddingHorizontal: SIZES.small
    },
    searchBtn: {
        width: 50,
        height: "100%",
        borderRadius: SIZES.medium,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.primary
    },
    searchImage: {
        resizeMode: "contain",
        marginTop: SIZES.xxLarge,
        width: SIZES.width -58,
        height: SIZES.height -300,
        opacity: 0.9
    }
});

export default styles;