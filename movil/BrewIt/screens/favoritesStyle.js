import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../constants";

const styles = StyleSheet.create({
    cartContainer: {
        flex: 1,
        backgroundColor: COLORS.lightWhite
    },
    upperRow: {
        flex: 1,
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        position: "absolute",
        top: SIZES.xxLarge,
        width: SIZES.width -44,
        marginTop: 22,
        zIndex: 999
    },
    cartText: {
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary,
        marginTop: 7
    },
});

export default styles;