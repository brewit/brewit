import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../constants/index"

const styles = StyleSheet.create({
   textStyle: {
    fontFamily: "bold",
    fontSize: 40 
   },
   appBarWrapper: {
    marginHorizontal: 22,
    marginTop: SIZES.small,
    
   },
   appBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
   },
   userWrapper: { 
      flexDirection: "row",
      alignItems: "center",
    },
    user: {
     fontFamily: "semibold",
     fontSize: SIZES.medium,
     color: COLORS.gray,
     marginLeft: 10
    },
   cartCount: {
    position: "absolute",
    bottom: 16,
    width: 16,
    height: 16,
    borderRadius: 8,
    alignItems: "center",
    backgroundColor: "green",
    justifyContent: "center",
    zIndex: 999
   },
   cartNumber: {
    fontFamily: "regular",
    fontWeight: "600",
    fontSize: 10,
    color: COLORS.lightWhite
   },
   brand: {
      fontFamily: "light",
      fontSize: SIZES.small,
      fontStyle: "italic",
      color: COLORS.gray,
      textAlign: "center",
      marginTop: 40
  }
});

export default styles;