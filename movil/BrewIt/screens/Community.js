import React, { useEffect, useState, useContext } from 'react';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback, SafeAreaView, Image, ScrollView } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from './communityStyle';
import { COLORS } from '../constants';
import axios from 'axios';
import baseURL from '../env';
import { CredentialsContext } from '../components/CredentialsContext';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import LikeComponent from '../components/post/likes/LikeComponent';

const Community = () => {
    const { storedCredentials } = useContext(CredentialsContext);
    const [posts, setPosts] = useState([]);
    const navigation = useNavigation();

    const fetchPosts = async () => {
        if (!storedCredentials.token) return;

        try {
            const response = await axios.get(`${baseURL}/api/posts`, {
                headers: {
                    Authorization: `Bearer ${storedCredentials.token}`,
                },
            });
            setPosts(response.data); // Actualizar la lista de posts
        } catch (e) {
            console.log('Failed to fetch posts', e);
        }
    };

    const updatePost = (updatedPost) => {
      setPosts((prevPosts) => {
          return prevPosts.map((post) => 
              post._id === updatedPost._id ? updatedPost : post
          );
      });
    };
  
  

    useFocusEffect(
      React.useCallback(() => {
          fetchPosts();
      }, [storedCredentials])
    );

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.community}>
                <Text style={styles.communityTitle}>Community</Text>
            </View>

            <ScrollView style={styles.scroll} contentContainerStyle={styles.container}>
            {posts.map(post => (
              <TouchableWithoutFeedback 
                key={post._id}
                onPress={() => navigation.navigate('PostDetails', { postId: post._id })}
              >
                <View key={post._id} style={styles.tweetContainer}>
                    <Image source={require("../assets/img/def.jpg")} style={styles.avatar} />

                    <View style={styles.contentContainer}>
                        <View style={styles.userInfoContainer}>
                            <Text style={styles.username}>{post.user.name}</Text>
                            {post.user.isAdmin && (
                                <Ionicons 
                                    name="checkmark-circle" 
                                    size={20} 
                                    color={COLORS.primary} 
                                    style={{ marginTop: -9, marginLeft: 2 }}
                                />
                            )}
                            <Text style={styles.email}>
                                @{post.user.email && post.user.email.split('@')[0]}
                            </Text>
                        </View>
                        <Text style={styles.post}>{post.content}</Text>

                        <View style={styles.iconContainer}>
                            <LikeComponent post={post} updatePost={updatePost} />

                            <TouchableOpacity 
                                style={[styles.iconButton, { flexDirection: 'row', alignItems: 'center' }]} 
                                onPress={() => navigation.navigate('PostDetails', { postId: post._id })}
                            >
                                <Ionicons name="chatbubble-outline" size={24} color={COLORS.gray2} />
                                <Text style={{ marginLeft: 5 }}>
                                    {post.commentCount > 0 ? post.commentCount : ""}
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.iconButton}>
                                <Ionicons name="share-social-outline" size={24} color={COLORS.gray2} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
              </TouchableWithoutFeedback>
            ))}
            </ScrollView>

            <View style={styles.addButtonContainer}>
                <TouchableOpacity onPress={() => navigation.navigate('NewPost')} style={styles.addButton}>
                    <Ionicons name="add-circle-outline" size={32} color="#fff" />
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

export default Community;


/*
import React from 'react';
import { View, Text, Image, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { COLORS } from '../constants';
import styles from './communityStyle';

const tweets = [
  {
    id: '1',
    user: 'John Doe',
    image: 'https://placeimg.com/100/100/people',
    content: 'Just started using this new social app! Loving it!',
  },
  {
    id: '2',
    user: 'Jane Smith',
    image: 'https://placeimg.com/100/100/animals',
    content: 'The weather is amazing today. Enjoying the sun!',
  },
  {
    id: '3',
    user: 'Sam Taylor',
    image: 'https://placeimg.com/100/100/nature',
    content: 'Workout completed for the day! Feeling great.',
  },
  {
    id: '4',
    user: 'Michael Lee',
    image: 'https://placeimg.com/100/100/tech',
    content: 'Does anyone have recommendations for good tech podcasts?',
  },
  {
    id: '5',
    user: 'Emma Williams',
    image: 'https://placeimg.com/100/100/arch',
    content: 'Throwback to my trip to Rome! Such an amazing place.',
  },
  {
    id: '6',
    user: 'Carlos M',
    image: 'https://placeimg.com/100/100/people',
    content: '¡El prototipo para fermentar es increíble! Obtuve resultados consistentes en cada lote.',
  },
  {
    id: '7',
    user: 'Lucía P',
    image: 'https://placeimg.com/100/100/people',
    content: 'Me encantó la calidad de los ingredientes. ¡Mi cerveza nunca había tenido un sabor tan fresco!',
  },
  {
    id: '8',
    user: 'Miguel O',
    image: 'https://placeimg.com/100/100/people',
    content: 'La relación calidad-precio es insuperable. Estoy más que satisfecho con mi compra.',
  },
  {
    id: '9',
    user: 'Alejandra S',
    image: 'https://placeimg.com/100/100/people',
    content: 'El sabor de esta cerveza es fenomenal. Todos mis amigos quedaron impresionados.',
  },
  {
    id: '10',
    user: 'Pedro L',
    image: 'https://placeimg.com/100/100/people',
    content: 'He probado otras marcas antes, pero ninguna se compara con esta en términos de calidad y sabor.',
  },
];

const Community = () => {
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.community}>
                <Text style={styles.communityTitle}>Community</Text>
            </View>

            <ScrollView style={styles.scroll} contentContainerStyle={styles.container}>
            {tweets.map((tweet) => (
                <View key={tweet.id} style={styles.tweetContainer}>
                    <Image source={require("../assets/img/def.jpg")} style={styles.avatar} />

                    <View style={styles.contentContainer}>
                        <Text style={styles.username}>{tweet.user}</Text>
                        <Text>{tweet.content}</Text>

                        <View style={styles.iconContainer}>
                            <TouchableOpacity style={styles.iconButton}>
                                <Ionicons name="heart-outline" size={26} color={COLORS.gray2} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.iconButton}>
                                <Ionicons name="chatbubble-outline" size={24} color={COLORS.gray2} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.iconButton}>
                                <Ionicons name="share-social-outline" size={24} color={COLORS.gray2} />
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            ))}
            </ScrollView>
        </SafeAreaView>
      );
};

export default Community;
*/



/*
import { TouchableOpacity, Text, View } from 'react-native';
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons, Fontisto } from "@expo/vector-icons";
import styles from './communityStyle';
import { COLORS } from '../constants';

const Community = () => {
  return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
          <View style={styles.community}>
              <Text style={styles.communityTitle}> Community </Text>
          </View>

          <View style={styles.container}>
              <Ionicons name="construct-outline" size={100} color={COLORS.gray2} />
              <Text style={styles.text}>This page is under maintenance. {"\n"}Come back later</Text>
          </View>
      </SafeAreaView>
  )
}

export default Community;
*/
