import React, { useState, useContext } from "react";
import baseURL from "../env";
import { StatusBar } from 'expo-status-bar';
import { Formik } from "formik";
import { Octicons, Ionicons, Fontisto } from '@expo/vector-icons';
import KeyboardAvoidingWrapper from "../components/KeyboardAvoidingWrapper";
import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CredentialsContext } from './../components/CredentialsContext';
import { 
    StyledContainer, 
    InnerContainer, 
    PageLogo, 
    PageTitle, 
    SubTitle, 
    StyledFormArea, 
    LeftIcon, 
    StyledInputLabel, 
    StyledTextInput, 
    RightIcon,
    StyledButton,
    ButtonText,
    Colors,
    MsgBox,
    Line,
    ExtraView,
    ExtraText,
    TextLink,
    TextLinkContent
} from './../components/styles';
import DateTimePicker from '@react-native-community/datetimepicker';
import { View, TouchableOpacity, ActivityIndicator } from "react-native";
import { useCart } from '../components/cart/CartContext';


const {brand, darkLight, primary} = Colors;

const Signup = ({navigation}) => {
    const [hidePassword, setHidePassword] = useState(true);
    const [show, setShow] = useState(false);
    const [date, setDate] = useState(new Date(2000, 0, 1))
    const [bdate, setBdate] = useState(); // actual birthdate to sent

    const [message, setMessage] = useState();
    const [messageType, setMessageType] = useState();

    // context
    const {storedCredentials, setStoredCredentials} = useContext(CredentialsContext);
    const {storeToken} = useCart();

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(false);
        setDate(currentDate);
        setBdate(currentDate);
    }
    const showDatePicker = () => {
        setShow(true);
    }

    // form handling
    const handleSignup = (credentials, setSubmitting) => {
        handleMessage(null);
        const url = `${baseURL}/api/users/signup`;

        // Validar que el número de teléfono tenga exactamente 10 dígitos
        if (!/^\d{10}$/.test(credentials._id)) {
            handleMessage("Phone number should have exactly 10 digits", 'FAILED');
            setSubmitting(false);
            return;
        }

        // date to String
        const date = new Date(credentials.birthdate);
        credentials.birthdate = date.toISOString().slice(0, 10);

        axios.post(url, credentials).then((response) => {
            const result = response.data;
            const {message, status, data} = result;

            if (status !== "SUCCESS"){
                handleMessage(message, status);
            }
            else {
                persistLogin({ ...data }, message, status, result.token);
                storeToken(result.token);
            }
            setSubmitting(false);
        })
        .catch(error => {
            console.log(error.toJSON());
            setSubmitting(false);
            handleMessage("An error occured. Check yout network and try again");
        })
    }

    const handleMessage = (message, type = 'FAILED') => {
        setMessage(message);
        setMessageType(type);
    }


    const persistLogin = (credentials, message, status, token) => {
        const dataToStore = {
            ...credentials,
            token: token,
        };
    
        AsyncStorage.setItem('brewItCredentials', JSON.stringify(dataToStore))
            .then(() => {
                handleMessage(message, status);
                setStoredCredentials(dataToStore);
            })
            .catch((error) => {
                console.log(error);
                handleMessage("Persisting login failed")
            });
    };


    return (
        <KeyboardAvoidingWrapper>
            <StyledContainer>
                <StatusBar style="dark" />
                <InnerContainer>
                    <PageTitle> BrewIt </PageTitle>
                    <SubTitle> Create an account </SubTitle>

                    {show && (
                        <DateTimePicker 
                            testID="dateTimePicker"
                            value={date}
                            mode="date"
                            is24Hour={true}
                            display="default"
                            onChange={onChange}
                        />
                    )}
                    
                    <Formik
                        initialValues={{name: '', email: '', _id: '', birthdate: '', password: '', confirmPassword: ''}}
                        onSubmit={(values, {setSubmitting}) => {
                            values = {...values, birthdate: bdate};
                            if (values.name == '' || 
                                values.email == '' ||
                                values._id == '' || 
                                values.birthdate == '' ||
                                values.password == '' || 
                                values.confirmPassword == ''
                                ){
                                handleMessage("Please fill all the fields");
                                setSubmitting(false);
                            } 
                            else if(values.password !== values.confirmPassword){
                                handleMessage("Passwords don't match");
                                setSubmitting(false);
                            }
                            else {
                                handleSignup(values, setSubmitting);
                            }
                        }}
                    >
                        {({handleChange, handleBlur, handleSubmit, values, isSubmitting}) => (
                            <StyledFormArea>

                                <MyTextInput 
                                    label="Full Name" 
                                    icon="person"
                                    placeholder="Alexis Vega"
                                    placeholderTextColor={darkLight}
                                    onChangeText={handleChange('name')}
                                    onBlur={handleBlur('name')}
                                    value={values.name}
                                />

                                <MyTextInput 
                                    label="Email" 
                                    icon="at"
                                    placeholder="example@gmail.com"
                                    placeholderTextColor={darkLight}
                                    onChangeText={handleChange('email')}
                                    onBlur={handleBlur('email')}
                                    value={values.email}
                                    keyboardType="email-address"
                                />

                                <MyTextInput 
                                    label="Phone" 
                                    icon="call"
                                    placeholder="6641234567"
                                    placeholderTextColor={darkLight}
                                    onChangeText={handleChange('_id')}
                                    onBlur={handleBlur('_id')}
                                    value={values._id}
                                    keyboardType="numeric"
                                    maxLength={10}
                                />

                                <MyTextInput 
                                    label="Birthdate" 
                                    icon="calendar"
                                    placeholder="YYYY - MM - DD"
                                    placeholderTextColor={darkLight}
                                    onChangeText={handleChange('birthdate')}
                                    onBlur={handleBlur('birthdate')}
                                    value={bdate ? bdate.toDateString() : ''}
                                    isDate={true}
                                    editable={false}
                                    showDatePicker={showDatePicker}
                                />

                                <MyTextInput 
                                    label="Password" 
                                    icon="lock-closed"
                                    placeholder="* * * * * * * *"
                                    placeholderTextColor={darkLight}
                                    onChangeText={handleChange('password')}
                                    onBlur={handleBlur('password')}
                                    value={values.password}
                                    secureTextEntry={hidePassword}
                                    isPassword={true}
                                    hidePassword={hidePassword}
                                    setHidePassword={setHidePassword}
                                />

                                <MyTextInput 
                                    label="Confirm Password" 
                                    icon="lock-closed"
                                    placeholder="* * * * * * * *"
                                    placeholderTextColor={darkLight}
                                    onChangeText={handleChange('confirmPassword')}
                                    onBlur={handleBlur('confirmPassword')}
                                    value={values.confirmPassword}
                                    secureTextEntry={hidePassword}
                                    isPassword={true}
                                    hidePassword={hidePassword}
                                    setHidePassword={setHidePassword}
                                />

                                <MsgBox type={messageType}> {message} </MsgBox>                                

                                {!isSubmitting && (
                                    <StyledButton onPress={handleSubmit}>
                                        <ButtonText> Signup </ButtonText>
                                    </StyledButton>
                                )}

                                {isSubmitting && (
                                    <StyledButton disabled={true}>
                                        <ActivityIndicator size="large" color={primary} />
                                    </StyledButton>
                                )}

                                <Line />

                                <ExtraView>
                                    <ExtraText> Already have an account? </ExtraText>
                                    <TextLink onPress={() => navigation.navigate("Login")}>
                                        <TextLinkContent> Login </TextLinkContent>
                                    </TextLink>
                                </ExtraView>

                            </StyledFormArea>
                        )}
                    </Formik>
                </InnerContainer>
            </StyledContainer>
        </KeyboardAvoidingWrapper>
    );
};

const MyTextInput = ({label, icon, isPassword, hidePassword, setHidePassword, 
                      isDate, showDatePicker, ...props}) => {
    return(
        <View>
            <LeftIcon>
                {/* <Octicons name={icon} size={30} color={brand} /> */}
                <Ionicons name={icon} size={30} color={brand} />
            </LeftIcon>
            
            <StyledInputLabel> {label} </StyledInputLabel>
            {!isDate && <StyledTextInput {...props} />}
            {isDate && <TouchableOpacity onPress={showDatePicker}>
                <StyledTextInput {...props} />
            </TouchableOpacity>}
            {isPassword && (
                <RightIcon onPress={() => setHidePassword(!hidePassword)}>
                    <Ionicons name={hidePassword ? 'md-eye-off' : 'md-eye'} size={30} color={darkLight} />
                </RightIcon>
            )}
        </View>
    )
}

export default Signup;