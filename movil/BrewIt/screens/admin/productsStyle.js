import { StyleSheet } from "react-native";
import { COLORS, SIZES, SHADOWS } from "../../constants";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.offwhite,
    },
    upperRow: {
        marginHorizontal: 20,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        top: SIZES.xxLarge,
        width: SIZES.width -44,
        marginTop: 10,
        zIndex: 999
    },
    productsText: {
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary,
        marginTop: 7
    },
    productsContainer: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 10,
        flexDirection: "row",
        padding: SIZES.medium,
        paddingTop: -8,
        paddingBottom: -8,
        borderRadius: SIZES.small+10,
        backgroundColor: "#FFF",
        ...SHADOWS.medium,
        shadowColor: COLORS.lightWhite,
    },
    image: {
        width: 70,
        backgroundColor: COLORS.secondary,
        borderRadius: SIZES.medium,
        justifyContent: "center",
        alignItems: "center"
    },
    productImg: {
        width: "100%",
        height: 70,
        borderRadius: SIZES.small,
        resizeMode: "cover"
    },
    productInfo: {
        flex: 1,
        marginHorizontal: SIZES.medium,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    name: {
        fontSize: SIZES.medium,
        fontFamily: "bold",
        color: COLORS.black,
        marginBottom: -7,
        marginTop: 5
    },
    email: {
        fontSize: SIZES.small+2,
        fontFamily: "regular",
        color: COLORS.gray,
        marginTop: 3,
    },
    birthdate: {
        fontSize: SIZES.small+2,
        fontFamily: "semibold",
        color: COLORS.gray,
    },
    input: {
        width: "100%",
        padding: 10,
        marginVertical: 10,
        borderWidth: 1,
        borderColor: COLORS.gray,
        borderRadius: 5
    },
    modalContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.offwhite,
        padding: SIZES.xxLarge,
    },
    modalHeader: {
        padding: SIZES.medium,
        borderColor: COLORS.lightGray,
        alignItems: 'center',
        backgroundColor: COLORS.offwhite
    },
    modalTitle: {
        fontSize: SIZES.xLarge+5,
        fontFamily: "bold",
        color: COLORS.primary,
        marginBottom: SIZES.large,
        alignSelf: "center"
    },
    modalContent: {
        backgroundColor: "white",
        width: '90%',
        borderRadius: SIZES.medium,
        padding: SIZES.large,
        ...SHADOWS.large,
        shadowColor: COLORS.lightWhite,
    },
    inputLabel: {
        fontSize: SIZES.medium,
        fontFamily: "semibold",
        color: "#000",
        marginBottom: SIZES.small,
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: COLORS.gray,
        marginBottom: SIZES.medium,
        fontFamily: "regular",
        color: COLORS.black,
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: "space-between",
        marginTop: SIZES.medium,
    },
    button: {
        flex: 0.48,
        borderRadius: 8,
        padding: SIZES.medium,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalButton: {
        backgroundColor: COLORS.primary,
    },
    cancelButton: {
        backgroundColor: COLORS.gray
    },
    imagePreview: {
        width: 200, 
        height: 200, 
        borderRadius: SIZES.medium,
        marginVertical: SIZES.medium,
    },     
    update: {
        backgroundColor: COLORS.yellow,
        borderRadius: 8,
        padding: 6,
        marginVertical: 3,
        marginTop: 8
    },
    delete: {
        backgroundColor: COLORS.primary,
        borderRadius: 8,
        padding: 6,
        marginVertical: 3,
        marginBottom: 8
    }
});

export default styles;