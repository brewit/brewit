import React, { useEffect, useState, useContext } from 'react';
import { 
  Text, 
  View, 
  TouchableOpacity, 
  SafeAreaView, 
  Image, 
  ScrollView, 
  Modal, 
  TextInput,
  Alert,
  Button,
  ToastAndroid
} from 'react-native';
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import * as ImagePicker from 'expo-image-picker';
import styles from './productsStyle';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';
import { COLORS } from '../../constants';

const Products = () => {
  const navigation = useNavigation();
  const { storedCredentials } = useContext(CredentialsContext);
  const [products, setProducts] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [newProductName, setNewProductName] = useState('');
  const [newProduct, setNewProduct] = useState({
    name: "",
    price: "",
    description: "",
    origin: "",
    photo: null
  });
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [updateModalVisible, setUpdateModalVisible] = useState(false);


  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      quality: 1
    });
  
    // Modificación: verifica si la respuesta tiene assets y no está cancelada
    if (result.assets && !result.canceled) {
      setNewProduct(prev => ({ ...prev, photo: result.assets[0].uri })); // Usa el uri del primer asset
    }
  };
  

  const createProduct = async () => {
    const formData = new FormData();
    formData.append('name', newProduct.name);
    formData.append('price', newProduct.price);
    formData.append('description', newProduct.description);
    formData.append('origin', newProduct.origin);
    if (newProduct.photo) {
      formData.append('photo', {
        uri: newProduct.photo,
        type: 'image/jpeg',
        name: 'photo.jpg'
      });
    }
  
    try {
      const response = await axios.post(`${baseURL}/api/products`, formData, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
          'Content-Type': 'multipart/form-data'
        }
      });
      ToastAndroid.show("Product created successfully!", ToastAndroid.SHORT);
      setModalVisible(false); 
      setNewProduct({
        name: "",
        price: "",
        description: "",
        origin: "",
        photo: null
      }); 
      fetchProducts(); 
    } catch (e) {
      ToastAndroid.show("Failed to create product", ToastAndroid.SHORT);
    }
  };

  const getImageUri = (path) => {
    if(path.startsWith('/uploads/')) {
        return `${baseURL}${path}`;
    }
    return path;
  }

  const handleEditProduct = (product) => {
    setSelectedProduct(product);
    setUpdateModalVisible(true);
  };

  const updateProduct = async () => {
    const formData = new FormData();
    formData.append('name', selectedProduct.name);
    formData.append('price', selectedProduct.price);
    formData.append('description', selectedProduct.description);
    formData.append('origin', selectedProduct.origin);

    // console.log("Valor de selectedProduct.photo:", selectedProduct.photo);
    // console.log("Resultado de getImageUri:", getImageUri(selectedProduct.photo));
    
    if (selectedProduct.photo) {
      if (typeof selectedProduct.photo === "string" && !selectedProduct.photo.startsWith(baseURL) && !selectedProduct.photo.startsWith('/uploads/')) {
          // Es una nueva imagen seleccionada
          console.log("Añadiendo nueva imagen seleccionada");
          formData.append('photo', {
              uri: selectedProduct.photo,
              type: 'image/jpeg',
              name: 'photo.jpg'
          });
      } else {
          // Aquí ya no añadas nada, ya que es la imagen original y no necesitas reenviarla
          console.log("Manteniendo imagen original");
      }
    }
  

    try {
        await axios.put(`${baseURL}/api/products/${selectedProduct._id}`, formData, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
          'Content-Type': 'multipart/form-data'
        }
      });
      ToastAndroid.show("Product updated successfully!", ToastAndroid.SHORT);
      setSelectedProduct(null);
      setUpdateModalVisible(false);
      fetchProducts();
    } catch (e) {
      ToastAndroid.show("Failed to update product", ToastAndroid.SHORT);
      console.log("Error al actualizar el producto:", e.response ? e.response.data : e.message);
    }
  };

  
  const pickImageForUpdate = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      quality: 1
    });
  
    if (result.assets && !result.canceled) {
      setSelectedProduct(prev => ({ ...prev, photo: result.assets[0].uri }));
    }
  };

  const deleteProduct = async (productId) => {
    try {
        await axios.delete(`${baseURL}/api/products/${productId}`, {
            headers: {
                Authorization: `Bearer ${storedCredentials.token}`,
            },
        });
        ToastAndroid.show("Product deleted successfully!", ToastAndroid.SHORT);
        fetchProducts();
    } catch (e) {
        console.log('Failed to delete product', e);
        ToastAndroid.show("Failed to delete product.", ToastAndroid.LONG);
    }
  };

  const promptDeleteProduct = (productId, productName) => {
      Alert.alert(
          "Delete Product",
          `Are you sure you want to delete ${productName}?`,
          [
              {
                  text: "Cancel",
                  style: "cancel"
              },
              {
                  text: "OK", onPress: () => deleteProduct(productId)
              }
          ]
      );
  };




  const fetchProducts = async () => {
    if (!storedCredentials.token) return;
  
    try {
      const response = await axios.get(`${baseURL}/api/products`, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
        },
      });
      setProducts(response.data); // Actualizar la lista de usuarios
    } catch (e) {
      console.log('Failed to fetch products', e);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, [storedCredentials]);



  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.upperRow}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="chevron-back-circle" size={30} />
        </TouchableOpacity>
        <Text style={styles.productsText}> Products </Text>
        <TouchableOpacity onPress={() => setModalVisible(true)}>
          <Ionicons name="add-circle" size={30} color={COLORS.green2} style={{ marginLeft: 150, marginTop: 5 }} />
        </TouchableOpacity>
      </View>

        <ScrollView style={{marginTop: 40, marginBottom: 20}}>
        {products.map(product => (
        <View key={product._id} style={styles.productsContainer}>
            <View style={styles.image}>
                <Image 
                    source={{ uri: getImageUri(product.photo) }}
                    style={styles.productImg}
                />
            </View>

            <View style={styles.productInfo}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.name}>{product.name}</Text>
                    <Text style={styles.origin}>{product.origin}</Text>
                    <Text style={styles.price}>${product.price}</Text>
                </View>
            </View>

            <View>
                <TouchableOpacity onPress={() => handleEditProduct(product)} style={styles.update}>
                  <MaterialCommunityIcons name="pencil-outline" size={28} color="black" />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => promptDeleteProduct(product._id, product.name)} style={styles.delete}>
                    <Ionicons name="trash-outline" size={28} color="#fff" />
                </TouchableOpacity>
            </View>
        </View>
        ))}
        </ScrollView>

        {/* CREATE */}  
        <Modal animationType="slide" visible={modalVisible}>
            <View style={styles.modalContainer}>
                <View style={styles.modalHeader}>
                    <Text style={styles.modalTitle}>Add New Product</Text>
                </View>
                <View style={styles.modalContent}>
                    <TextInput
                        placeholder="Name"
                        value={newProduct.name}
                        onChangeText={(text) => setNewProduct(prev => ({ ...prev, name: text }))}
                        style={styles.input}
                    />
                    <TextInput
                        placeholder="Price"
                        value={newProduct.price}
                        onChangeText={(text) => setNewProduct(prev => ({ ...prev, price: text }))}
                        keyboardType="phone-pad"
                        style={styles.input}
                    />
                    <TextInput
                        placeholder="Description"
                        value={newProduct.description}
                        onChangeText={(text) => setNewProduct(prev => ({ ...prev, description: text }))}
                        style={styles.input}
                    />
                    <TextInput
                        placeholder="Origin"
                        value={newProduct.origin}
                        onChangeText={(text) => setNewProduct(prev => ({ ...prev, origin: text }))}
                        style={styles.input}
                    />

                    <Button title="Pick an image" onPress={pickImage} />

                    {newProduct.photo && 
                        <Image 
                            source={{ uri: newProduct.photo }} 
                            style={styles.imagePreview} 
                        />
                    }

                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity style={[styles.button, styles.modalButton]} onPress={createProduct}>
                            <Text style={{ fontFamily: "bold", color: COLORS.white, fontSize: 16 }}>Create</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.button, styles.cancelButton]} onPress={() => setModalVisible(false)}>
                            <Text style={{ fontFamily: "bold", color: COLORS.white, fontSize: 16 }}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>


        {/* UPDATE */}  
        <Modal animationType="slide" visible={updateModalVisible}>
            <View style={styles.modalContainer}>
              <View style={styles.modalHeader}>
                <Text style={styles.modalTitle}>Update Product</Text>
              </View>
              <View style={styles.modalContent}>
                <Text style={styles.inputLabel}>Name: </Text>
                <TextInput
                  placeholder="Name"
                  value={selectedProduct?.name}
                  onChangeText={(text) => setSelectedProduct(prev => ({ ...prev, name: text }))}
                  style={styles.textInput}
                />

                <Text style={styles.inputLabel}>Price: </Text>
                <TextInput
                  placeholder="Price"
                  value={selectedProduct?.price}
                  onChangeText={(text) => setSelectedProduct(prev => ({ ...prev, price: text }))}
                  keyboardType="phone-pad"
                  style={styles.textInput}
                />

                <Text style={styles.inputLabel}>Description: </Text>
                <TextInput
                  placeholder="Description"
                  value={selectedProduct?.description}
                  onChangeText={(text) => setSelectedProduct(prev => ({ ...prev, description: text }))}
                  style={styles.textInput}
                />

                <Text style={styles.inputLabel}>Origin: </Text>
                <TextInput
                  placeholder="Origin"
                  value={selectedProduct?.origin}
                  onChangeText={(text) => setSelectedProduct(prev => ({ ...prev, origin: text }))}
                  style={styles.textInput}
                />

                <Button title="Pick an image" onPress={pickImageForUpdate} />

                {selectedProduct?.photo && 
                  <Image 
                    source={{ uri: getImageUri(selectedProduct.photo) }} 
                    style={styles.imagePreview} 
                  />
                }

                <View style={styles.buttonsContainer}>
                  <TouchableOpacity style={[styles.button, styles.modalButton]} onPress={updateProduct}>
                    <Text style={{ fontFamily: "bold", color: COLORS.white, fontSize: 16 }}>Update</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={[styles.button, styles.cancelButton]} onPress={() => setUpdateModalVisible(false)}>
                    <Text style={{ fontFamily: "bold", color: COLORS.white, fontSize: 16 }}>Cancel</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
        </Modal>
    </SafeAreaView>
  );
};

export default Products;
