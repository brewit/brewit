import { Text, View, SafeAreaView, TouchableOpacity } from 'react-native';
import React, { useContext } from 'react';
import { useNavigation } from '@react-navigation/native';
import { CredentialsContext } from '../../components/CredentialsContext';
import styles from './adminStyle';

const Admin = () => {
    const navigation = useNavigation();
    const {storedCredentials, setStoredCredentials} = useContext(CredentialsContext);
    const { name } = storedCredentials;

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.admin}>
                <Text style={styles.adminTitle}> Admin Panel </Text>
            </View>

            <Text style={styles.adminName}>Hi, {name}</Text>

            <View style={styles.buttonsContainer}>
                <TouchableOpacity style={styles.usersBtn} onPress={() => navigation.navigate("Users")}>
                    <Text style={styles.buttonText}> Users </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.productsBtn} onPress={() => navigation.navigate("Products")}>
                    <Text style={styles.buttonText}> Products </Text>
                </TouchableOpacity>
            </View>

        </SafeAreaView>
    )
}

export default Admin;