import React, { useEffect, useState, useContext } from 'react';
import { 
  Text, 
  View, 
  TouchableOpacity, 
  SafeAreaView, 
  Image, 
  ScrollView, 
  Alert, 
  ToastAndroid, 
  Modal, 
  TextInput, 
  Button,
  Switch
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { COLORS, SIZES } from '../../constants';
import styles from './usersStyle';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import baseURL from '../../env';
import { CredentialsContext } from '../../components/CredentialsContext';

const Users = () => {
  const navigation = useNavigation();
  const { storedCredentials } = useContext(CredentialsContext);
  const [users, setUsers] = useState([]);
  const [isModalVisible, setModalVisible] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const [isAdminChecked, setIsAdminChecked] = useState(false);
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date(selectedUser?.birthdate || "2000-01-01"));
  const [bdate, setBdate] = useState();


  const formatDateToLocalISOString = (dateObj) => {
    let month = String(dateObj.getMonth() + 1);
    let day = String(dateObj.getDate());
    const year = String(dateObj.getFullYear());
    
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return `${year}-${month}-${day}`;
  }

  const onChange = (event, selectedDate) => {
    if (event.type === "dismissed") {
        setShow(false);
        return;
    }

    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
    setSelectedUser(prev => ({...prev, birthdate: formatDateToLocalISOString(currentDate)}));
  }

  useEffect(() => {
    if (selectedUser?.birthdate) {
        setDate(new Date(selectedUser.birthdate));
    }
  }, [selectedUser]);


  const showDatePicker = () => {
      setShow(true);
  }

  const fetchUsers = async () => {
    if (!storedCredentials.token) return;
  
    try {
      const response = await axios.get(`${baseURL}/api/users`, {
        headers: {
          Authorization: `Bearer ${storedCredentials.token}`,
        },
      });
      setUsers(response.data); // Actualizar la lista de usuarios
    } catch (e) {
      console.log('Failed to fetch users', e);
    }
  };

  useEffect(() => {
    fetchUsers();
  }, [storedCredentials]);


  const openUpdateModal = (user) => {
    setSelectedUser(user);
    setIsAdminChecked(user.isAdmin);
    setModalVisible(true);
  };


  const updateUser = async () => {
      if(!selectedUser) return;
      
      try {
          await axios.put(`${baseURL}/api/users/${selectedUser._id}`, selectedUser, {
              headers: {
                  Authorization: `Bearer ${storedCredentials.token}`,
              },
          });
          ToastAndroid.show("User updated successfully!", ToastAndroid.SHORT);
          fetchUsers();
          setModalVisible(false);  // Cierra el modal después de la actualización
      } catch (e) {
          console.log('Failed to update user', e);
          ToastAndroid.show("Failed to update user.", ToastAndroid.LONG);
      }
  };



  const deleteUser = async (userId) => {
    try {
        await axios.delete(`${baseURL}/api/users/${userId}`, {
            headers: {
                Authorization: `Bearer ${storedCredentials.token}`,
            },
        });
        ToastAndroid.show("User deleted successfully!", ToastAndroid.SHORT);
        fetchUsers();  // Vuelve a obtener la lista de usuarios para reflejar el cambio
    } catch (e) {
        console.log('Failed to delete user', e);
        ToastAndroid.show("Failed to delete user.", ToastAndroid.LONG);
    }
  };

  const promptDelete = (userId, userName) => {
    Alert.alert(
        "Delete User",
        `Are you sure you want to delete ${userName}?`,  // Aquí insertamos el nombre del usuario en el mensaje.
        [
            {
                text: "Cancel",
                style: "cancel"
            },
            {
                text: "OK", onPress: () => deleteUser(userId)
            }
        ]
    );
  };



  return (
      <SafeAreaView style={styles.container}>
        <View style={styles.upperRow}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons name="chevron-back-circle" size={30} />
          </TouchableOpacity>
          <Text style={styles.usersText}> Manage Users </Text>
        </View>

          <ScrollView style={{marginTop: 40, marginBottom: 20}}>
          {users.map(user => (
          <View key={user._id} style={styles.usersContainer}>
              <View style={styles.image}>
                  <Image 
                      source={require('../../assets/img/def.jpg')}
                      style={styles.userImg}
                  />
              </View>

              <View style={styles.userInfo}>
                  <View style={{ flex: 1 }}>
                      <View style={styles.userInfoContainer}>
                          <Text style={styles.name}>{user.name}</Text>
                          {user.isAdmin && (
                              <Ionicons 
                                  name="checkmark-circle" 
                                  size={20} 
                                  color={COLORS.primary} 
                                  style={{ marginTop: 8, marginLeft: 2 }}
                              />
                          )}
                      </View>
                      <Text style={styles.email}>{user.email}</Text>
                      <Text style={styles.birthdate}>{user.birthdate}</Text>
                  </View>
              </View>

              <View>
                  <TouchableOpacity onPress={() => openUpdateModal(user)} style={styles.update}>
                      <MaterialCommunityIcons name="pencil-outline" size={28} color="black" />
                  </TouchableOpacity>
                  
                  <TouchableOpacity onPress={() => promptDelete(user._id, user.name)} style={styles.delete}>
                      <Ionicons name="trash-outline" size={28} color="#fff" />
                  </TouchableOpacity>

              </View>
          </View>
          ))}
          </ScrollView>

          <Modal
              animationType="slide"
              transparent={false}
              visible={isModalVisible}
          >
              <View style={styles.modalContainer}>
                  <View style={styles.modalHeader}>
                      <Text style={styles.modalTitle}>Modify User</Text>
                  </View>
                  <View style={styles.modalContent}>
                      <Text style={styles.inputLabel}>Phone: </Text>
                      <Text style={styles.textPhoneInput}>{selectedUser?._id}</Text>

                      <Text style={styles.inputLabel}>Name: </Text>
                      <TextInput
                          style={styles.textInput}
                          placeholder="Name"
                          value={selectedUser?.name}
                          onChangeText={(text) => setSelectedUser(prev => ({...prev, name: text}))}
                      />

                      <Text style={styles.inputLabel}>Email: </Text>
                      <TextInput
                          style={styles.textInput}
                          placeholder="Email"
                          value={selectedUser?.email}
                          onChangeText={(text) => setSelectedUser(prev => ({...prev, email: text}))}
                      />

                      <Text style={styles.inputLabel}>Birthdate: </Text>
                      <TouchableOpacity onPress={showDatePicker}>
                          <Text style={styles.textInput}>{bdate ? bdate.toISOString().slice(0, 10) : selectedUser?.birthdate}</Text>
                      </TouchableOpacity>

                      {show && (
                          <DateTimePicker 
                              testID="dateTimePicker"
                              value={date}
                              mode="date"
                              is24Hour={true}
                              display="default"
                              onChange={onChange}
                          />
                      )}

                      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: SIZES.medium }}>
                          <Text style={styles.inputLabel}>Is Admin? </Text>
                          <Switch
                              value={isAdminChecked}
                              onValueChange={(newValue) => {
                                  setIsAdminChecked(newValue);
                                  setSelectedUser(prev => ({...prev, isAdmin: newValue}));
                              }}
                              style={{ marginTop: -15, marginLeft: 120 }}
                          />
                      </View>

                      <View style={styles.buttonsContainer}>
                          <TouchableOpacity style={[styles.button, styles.updateButton]} onPress={updateUser}>
                              <Text style={{ fontFamily: "bold", color: "#fff", fontSize: 16 }}>Update</Text>
                          </TouchableOpacity>

                          <TouchableOpacity style={[styles.button, styles.closeButton]} onPress={() => setModalVisible(false)}>
                              <Text style={{ fontFamily: "bold", color: COLORS.white, fontSize: 16 }}>Close</Text>
                          </TouchableOpacity>
                      </View>
                  </View>
              </View>
          </Modal>
      </SafeAreaView>
  );
};

export default Users;
