import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    admin: {
        marginTop: SIZES.xxLarge-2,
        justifyContent: "center",
        alignItems: "center"
    },
    adminTitle:{
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary
    },
    adminName: {
        textAlign: "center",
        fontFamily: "semibold",
        fontSize: SIZES.large,
        color: COLORS.gray
    },
    buttonsContainer: {
        width: '100%',
        justifyContent: "center",
        alignItems: "center",
        marginTop: SIZES.normal,
    },
    usersBtn: {
        backgroundColor: COLORS.primary,
        borderRadius: 20,
        paddingHorizontal: SIZES.large,
        paddingVertical: SIZES.normal,
        width: '80%', 
        alignItems: "center",
        marginVertical: 10,
    },
    productsBtn: {
        backgroundColor: COLORS.primary,
        borderRadius: 20,
        paddingHorizontal: SIZES.large,
        paddingVertical: SIZES.normal,
        width: '80%',
        alignItems: "center",
        marginVertical: 10,
    },
    buttonText: {
        fontFamily: "semibold",
        fontSize: SIZES.large,
        color: "#fff",
    },
});


export default styles;