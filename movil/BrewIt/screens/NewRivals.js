import { TouchableOpacity, Text, View } from 'react-native';
import React from 'react';
import { SafeAreaView } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import styles from './newRivalsStyle';
import { COLORS } from '../constants';
import ProductList from '../components/products/ProductList';

const NewRivals = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.wrapper}>
                <View style={styles.upperRow}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Ionicons name="chevron-back-circle" size={30} color={COLORS.black} />
                    </TouchableOpacity>

                    <Text style={styles.heading}> Products </Text>
                </View>
                <ProductList/>
            </View>
        </SafeAreaView>
    )
}

export default NewRivals;