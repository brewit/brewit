import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../constants";

const styles = StyleSheet.create({
    img1: {
        height: 40,
        width: 40,
        borderRadius: 10,
        position: "absolute",
        top: -45,
        transform: [
            {translateX: 20},
            {translateY: 50},
            {rotate: "15deg"}
        ]
    },
    img2: {
        height: 90,
        width: 90,
        borderRadius: 20,
        position: "absolute",
        top: 10,
        left: 12,
        transform: [
            {translateX: 20},
            {translateY: 50},
            {rotate: "-15deg"}
        ]
    },
    img3: {
        height: 100,
        width: 100,
        borderRadius: 20,
        position: "absolute",
        top: -30,
        left: 100,
        transform: [
            {translateX: 50},
            {translateY: 50},
            {rotate: "-5deg"}
        ]
    },
    img4: {
        height: 100,
        width: 100,
        borderRadius: 20,
        position: "absolute",
        top: 130,
        left: -50,
        transform: [
            {translateX: 50},
            {translateY: 50},
            {rotate: "15deg"}
        ]
    },
    img5: {
        height: 200,
        width: 200,
        borderRadius: 20,
        position: "absolute",
        top: 110,
        left: 100,
        transform: [
            {translateX: 50},
            {translateY: 50},
            {rotate: "-15deg"}
        ]
    },
    container: {
        paddingHorizontal: 22,
        position: "absolute",
        top: 400,
        width: "100%",
        //alignItems: "center"
    },
    title: {
        fontFamily: "regular",
        fontSize: 60,
        fontWeight: 800,
        color: "#fff"
    },
    title2: {
        fontFamily: "regular",
        fontSize: 40,
        fontWeight: 800,
        color: "#fff",
        marginTop: -10
    },
    textContainer: {
        marginVertical: 32,
        marginTop: 80
    },
    subtitle: {
        fontFamily: "medium",
        fontSize: 16,
        color: "#fff",
        marginVertical: 4
    },
    brand: {
        fontWeight: "bold",
        //fontStyle: "italic"
    },
    subtitle2: {
        fontFamily: "light",
        fontStyle: "italic",
        fontSize: 13,
        color: "#fff",
        marginTop: -7
    },
    buttonContainer: {
        width: '100%',
        alignItems: 'center',
    },
    button: {
        marginTop: SIZES.large+2,
        width: "45%",
        paddingBottom: 12,
        paddingVertical: 10,
        borderColor: COLORS.primary,
        borderWidth: 2,
        borderRadius: SIZES.small,
        backgroundColor: "#fff",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    buttonText: {
        fontFamily: "bold",
        fontSize: SIZES.medium+2,
        color: COLORS.primary,
        textAlign: "center",
        marginTop: 5
    }
});

export default styles