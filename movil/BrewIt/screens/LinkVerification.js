import React, { useState, useEffect } from "react";
import { View } from "react-native";
import { StatusBar } from "expo-status-bar";
import { 
    StyledContainer, 
    TopHalf, 
    BottomHalf, 
    IconBg, 
    PageTitle, 
    InfoText, 
    EmphasizeText,
    StyledButton,
    ButtonText,
    InLineGroup,
    TextLink,
    TextLinkContent,
    Colors
} from "../components/styles";
import { Octicons, Ionicons } from '@expo/vector-icons';
import ResendTimer from './../components/ResendTimer';

const { brand, primary, green } = Colors;

const Verification = () => {
    const [resendingEmail, setResendingEmail] = useState(false);
    const [resendStatus, setResendStatus] = useState('Resend');

    // resend timer
    const [timeLeft, setTimeLeft] = useState(null);
    const [targetTime, setTargetTime] = useState(null);

    const [activeResend, setActiveResend] = useState(false);
    let resendTimerInterval;

    const calculateTimeLeft = (finalTime) => {
        const difference = finalTime - +new Date();
        
        if (difference >= 0){
            setTimeLeft(Math.round(difference / 1000));
        }
        else {
            setTimeLeft(null);
            clearInterval(resendTimerInterval);
            setActiveResend(true);
        }
    };

    const triggerTimer = (targetTimeinSeconds = 30) => {
        setTargetTime(targetTimeinSeconds);
        setActiveResend(false);
        const finalTime = +new Date() + targetTimeinSeconds * 1000;
        resendTimerInterval = setInterval(() => (
            calculateTimeLeft(finalTime), 1000
        ))
    }

    useEffect(() => {
        triggerTimer();

        return () => {
            clearInterval(resendTimerInterval);
        }
    }, []);

    const resendEmail = async () => {

    }

    return(
        <StyledContainer style={{ alignItems: 'center' }}>

            <TopHalf>
                <IconBg>
                    <StatusBar style="dark" />
                    {/* <Octicons name="mail" size={125} color={brand} /> */}
                    <Ionicons name="mail-open" size={125} color={brand} />
                </IconBg>
            </TopHalf>

            <BottomHalf>
                <PageTitle style={{ fontSize: 25 }}> Account Verification </PageTitle>
                <InfoText>
                    Please verify your email using the link sent to 
                    <EmphasizeText> {`test.email@gmail.com`} </EmphasizeText>
                </InfoText>
                <StyledButton
                    onPress={() => {}}
                    style={{ backgroundColor: green, flexDirection: 'row' }}
                >
                    <ButtonText> Proceed </ButtonText>
                    <Ionicons name="arrow-forward-circle" size={25} color={primary} />
                </StyledButton>

                <ResendTimer 
                    activeResend={activeResend}
                    resendingEmail={resendingEmail}
                    resendStatus={resendStatus}
                    timeLeft={timeLeft}
                    targetTime={targetTime}
                    resendEmail={resendEmail}
                />

            </BottomHalf>

        </StyledContainer>
    )
}

export default Verification;