import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../constants";

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: 60
    },    
    community: {
        marginTop: SIZES.medium,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: '#fff',
        zIndex: 1
    },
    communityTitle:{
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary,
        marginTop: 25
    },
    container: {
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#fff',
        //marginTop: -100
    },
    text: {
        textAlign: 'center',
        fontFamily: "bold",
        fontSize: 30,
        marginBottom: 20,
        color: COLORS.gray2
    },
    tweetContainer: {
        flexDirection: 'row',
        padding: 15,
        borderBottomWidth: 0.5,
        borderColor: COLORS.gray2,
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 10,
    },
    contentContainer: {
        flex: 1,
    },
    userInfoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    username: {
        fontFamily: 'bold',
        marginBottom: 5,
    },
    email: {
        marginLeft: 5,
        marginTop: -10,
        color: COLORS.gray,
        fontSize: SIZES.small+2,
    },
    post: {
        fontFamily: "regular"
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
    },
    iconButton: {
        marginRight: 95,
    },
    addButtonContainer: {
        position: 'absolute',
        bottom: 90, 
        right: 20, 
        zIndex: 1,
    },
    addButton: {
        backgroundColor: COLORS.primary,
        borderRadius: 30,
        padding: 10
    }
});

export default styles;