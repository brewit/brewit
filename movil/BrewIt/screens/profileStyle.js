import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../constants";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.lightWhite
    },
    profile: {
        marginTop: SIZES.medium,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 42,
        marginBottom: -20
    },
    profileTitle:{
        fontFamily: "bold",
        fontSize: SIZES.xLarge+5,
        color: COLORS.primary
    },
    info: {
        justifyContent: "center",
        alignItems: "center"
    },
    name: {
        fontFamily: "semibold",
        fontSize: SIZES.medium+2
    },
    email: {
        fontFamily: "regular",
        fontSize: SIZES.medium-1
    },
    line: {
        height: 1,
        width: "100%",
        backgroundColor: COLORS.gray2,
        marginVertical: 10,
        marginTop: 60,
        marginLeft: 20
    },
    line2: {
        height: 1,
        width: "100%",
        backgroundColor: COLORS.gray2,
        marginVertical: 10,
        marginTop: 15,
        marginBottom: 15
    },
    sections: {
        marginHorizontal: 22,
        marginTop: 40
    },
    favorites: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: 10
    },
    favoritesText: {
        fontFamily: "regular",
        fontSize: SIZES.medium,
        color: COLORS.gray,
        marginLeft: 10
    },
    orders: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: 10
    },
    ordersText: {
        fontFamily: "regular",
        fontSize: SIZES.medium,
        color: COLORS.gray,
        marginLeft: 7
    },
    cart: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: 10
    },
    cartText: {
        fontFamily: "regular",
        fontSize: SIZES.medium,
        color: COLORS.gray,
        marginLeft: 10
    },
    cache: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: 10
    },
    cacheText: {
        fontFamily: "regular",
        fontSize: SIZES.medium,
        color: COLORS.gray,
        marginLeft: 9
    },
    startBrewing: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: 10
    },
    startBrewingText: {
        fontFamily: "regular",
        fontSize: SIZES.medium,
        color: COLORS.gray,
        marginLeft: 7
    },
    logout: {
        padding: 15,
        width: "100%",
        backgroundColor: COLORS.primary,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        marginVertical: 5,
        height: 60,
        marginLeft: 20,
        marginTop: 45
    },
    brand: {
        fontFamily: "light",
        fontSize: SIZES.small,
        fontStyle: "italic",
        color: COLORS.gray,
        textAlign: "center",
        marginLeft: 40,
        marginTop: 21
    }
});

export default styles;